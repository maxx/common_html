(function($) {
	$('.countdown').countdown('2015/06/21').on('update.countdown', function (event) {
		$(this).html(event.strftime('<span>%D<span>days</span></span><b>:</b>'+'<span>%H<span>hours</span></span><b>:</b>'+'<span>%M<span>mins</span></span><b>:</b>'+'<span>%S<span>secs</span></span>'));
	});

	$('.questions .collapse').on('shown.bs.collapse', function () {
		$('.questions a[aria-expanded="true"]').each(function(){
			$(this).html('&minus;');
		});
	});

	$('.questions .collapse').on('hidden.bs.collapse', function () {
		$('.questions a[aria-expanded="false"]').each(function(){
			$(this).text('+');
		});
	});

	$('.has-error [data-toggle="tooltip"]').tooltip('show');

	smoothScroll.init({
		speed: 500,
		easing: 'easeInOutCubic',
		updateURL: true
	});

	/*var resizeText = function() {
		if ($(window).width() > 768) {
			if ($(".register h1 .pxg-source").length) {
				$(".register h1").removeClass("sn-pxg").remove('.pxg-set').removeAttr('style').html($(".register .pxg-source").html());
			}

			$(".register h1").height($(".register h1").height()).pxgradient({
				step: 2,
				colors: ['#fff', '#bbb'],
				dir: "y"
			});
		}
	}*/

	$('.video-frame').fitVids();
	$('.modal .modal-dialog').css('line-height', $(window).height()+'px');

	// $('#modal-video').on('show.bs.modal', function () {
	// 	var src = $(this).find('.video-frame').find('iframe').attr('src').replace("autoplay=0", "autoplay=1");
	// 	$(this).find('.video-frame').find('iframe').attr('src', src);
	// }).on('hide.bs.modal', function () {
	// 	var src = $(this).find('.video-frame').find('iframe').attr('src').replace("autoplay=1", "autoplay=0");
	// 	$(this).find('.video-frame').find('iframe').attr('src', src);
	// });

	$('a.video').prettyPhoto({default_width: 740,  default_height: 420,});
   	var popup_cfg = {};
   	if ($(window).width() < 400)  popup_cfg ={default_width: 250, default_height: 200,allow_resize: false};
    $('a.play').prettyPhoto(popup_cfg);

	$(window).load(function(){
		// resizeText();
	});

	$(window).resize(function () {
		$('.has-error [data-toggle="tooltip"]').tooltip('show');
		$('.modal .modal-dialog').css('line-height', $(window).height()+'px');
		// resizeText();
	});

}(jQuery));