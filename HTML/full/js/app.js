;
$(document).ready(function() {

    setTimeout(function() {
        $('#progressbar').circleProgress({
            size: 114,
            fill: {
                gradient: ["#f36d21", "#ff3408"]
            },
            reverse: true,
            startAngle: -Math.PI / 2,
            thickness: 18
        });

        $('.progressbar-number').each(function () {
            $(this).show();
            var $this = $(this);
            $({ Counter: 0 }).animate({ Counter: $this.text() }, {
                duration: 1000,
                easing: 'linear',
                step: function () {
                    $this.text(Math.ceil(this.Counter));
                }
            });
        });
    }, 100);

    $('.stats-diagram-tab-nav a').click(function(event) {
        event.preventDefault();
        if (!$(this).hasClass('active') && !$('.stats-diagram-tab-pane').is(':animated')) {
            $('.stats-diagram-tab-nav a').removeClass('active');
            $(this).addClass('active');
            var _this = $(this);
            $('.stats-diagram-tab-pane').fadeOut(150);
            setTimeout(function() {
                $(_this.attr('href')).fadeIn(150);
            }, 150);
        }
    });

});


