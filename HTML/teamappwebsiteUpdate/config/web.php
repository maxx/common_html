<?php
$params = require(__DIR__ . '/params.php');
switch (YII_ENV) {
    case 'local':
        $db = '/db_local.php';
        break;
    case 'dev':
        $db = '/db_dev.php';
        break;
    default:
        $db = '/db_prod.php';
        break;
}

$config = [
    'id' => 'watchapp',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            //'baseUrl'=>'/practicereporting',
            'cookieValidationKey' => 'AQfuHqn77E24KLuwSUwyMdLGdTl',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser', // required for POST input via `php://input`
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['user/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'sendGrid' => [
            'class' => 'bryglen\sendgrid\Mailer',
            'username' => 'shottrackerphp',
            'password' => 'H00pDr3am51',
            'options' => [
                'turn_off_ssl_verification' => true
            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . $db),
        'urlManager' => [
            // 'urlFormat' => 'path',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/urlrules.php'),
        ],
    ],
    'params' => $params,
];

if (YII_ENV==='local') {
    error_reporting(E_ALL|E_STRICT) ;

    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['modules']['debug']['allowedIPs'] = ['127.0.0.1', '::1'];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
