function lg(e) {
    return console.log(e);
}
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

Vue.prototype.$http = axios;

Vue.use(VeeValidate);
Vue.directive('popup-click', function (el, binding) {
    var elObj = $(el);
    elObj.on('click', function (event) {
        event.preventDefault();
        $.fancybox.close();
    });
});
Vue.directive('checkbox-uploaded', function (el, binding) {
    var elObj = $(el);
    elObj.change(function () {
        var isChecked = elObj.is(':checked');
        var rowParent = elObj.parents('.multi-upload');
        var inputObj = rowParent.find("input[type='file']");

        if (isChecked) {
            inputObj.attr('disabled', true);
            inputObj.val('');
            rowParent.find('.list-names').text('');
            app.$validator.fields.items.forEach(function (item) {
                if (item.name === elObj.data('target')) {
                    item.flags.valid = true;
                }
            });
        } else {
            if (app) {
                app.$validator.fields.items.forEach(function (item) {
                    if (item.name === elObj.data('target')) {
                        item.flags.valid = null;
                    }
                });
            }
            //inputObj.attr('disabled', false);
        }
    });
});

Vue.directive('upload-image', function (el, binding) {
    var elObj = $(el);
    var targetObj = elObj;
    var inputObj = elObj.find("input[type='file']");
    var labelObj = elObj.find("label");
    labelObj.attr('for', inputObj.attr('name'));
    inputObj.attr('id', labelObj.attr('for'));
    inputObj.change(function () {
    });
});

var formcontactgroup = Vue.extend({
    template: '#template-contact-form-group',
    props: {list: {type: Array}, type: {type: String}, isRequired: {type: Boolean, default: true}, elName: {type: String}, indexId: {type: Number}},
    inject: {
        $validator: '$validator'
    },
    data: function () {
        return {
            itemList: this.list,
            itemV: null,
        }
    },
    methods: {
        addAnother: function (list) {
            this.$parent.addAnother(list, this.type);
        },
        removeAnother: function (item, list) {
            list.splice(list.indexOf(item), 1);
        },
        updateValue: function (item) {
            var vm = this;
            vm.itemV = item;
        },
        check: function () {
            var vm = this;
            var hasOneDirty = false;
            vm.list.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    if (itemObject[propertyName]) {
                        hasOneDirty = true;
                    }
                }
            });
            return hasOneDirty;
        },
    },
    computed: {
        isRequiredDependenceOnOtherFilledFields: function () {
            var vm = this;
            var hasD = vm.itemV;
            var hasOneDirty = vm.check();
            if (hasOneDirty || vm.isRequired) {
                return true;
            } else {
                return false;
            }
        },
        isGroupValid: function () {
            var vm = this;
            var isValid = true;
            var hasOneDirty = vm.check();
            vm.list.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    isValid =
                            vm.fields.$step1
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']']
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']'].valid
                            && isValid;
                }
            });

            //If a section is not filled out, but not required, no green arrow should show up
            if (isValid && hasOneDirty)
                return isValid;
            else
                return null;
        }
    }
});

var formcontactaddressgroup = Vue.extend({
    template: '#template-contact-address-form-group',
    props: {list: {type: Array}, type: {type: String}, isRequired: {type: Boolean, default: true}, elName: {type: String}, indexId: {type: Number}},
    inject: {
        $validator: '$validator'
    },
    data: function () {
        return {
            //itemList: this.list,
            itemV: null,
        }
    },
    methods: {
        addAnother: function (list) {
            this.$parent.addAnother(list, this.type);
        },
        removeAnother: function (item, list) {
            list.splice(list.indexOf(item), 1);
        },
        updateValue: function (item) {
            var vm = this;
            vm.itemV = item;
        },
        check: function () {
            var vm = this;
            var hasOneDirty = false;
            vm.list.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    if (itemObject[propertyName]) {
                        hasOneDirty = true;
                    }
                }
            });
            return hasOneDirty;
        },
    },
    computed: {
        itemList: function () {
            lg('formcontactaddressgroup itemList');
            return this.list;
        },
        isRequiredDependenceOnOtherFilledFields: function () {
            var vm = this;
            var hasD = vm.itemV;
            var hasOneDirty = vm.check();
            if (hasOneDirty || vm.isRequired) {
                return true;
            } else {
                return false;
            }
        },
        isGroupValid: function () {
            var vm = this;
            var isValid = true;
            vm.itemList.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    isValid =
                            vm.fields.$step1
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']']
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']'].valid
                            && isValid;
                }
            });
            return isValid;
        }
    }
});

var formuploadimage = Vue.extend({
    template: '#template-form-upload-image',
    props: {list: {type: Array}, item: {}, elName: {type: String}, numRequired: {type: Number, default: 1}},
    inject: {
        $validator: '$validator'
    },
    data: function () {
        return {
            validCnt: 0,
            isFileChanged: false,
            // currentList: this.list,
            limitUpload: 11 // 12 inputs per group
        }
    },
    watch: {
        validCnt: function () {
            var vm = this;
            if (vm.validCnt >= vm.numRequired) {
                if (vm.currentList.length === vm.validCnt && vm.currentList.length <= vm.limitUpload) {
                    var id = Math.max.apply(Math, vm.currentList.map(function (c) {
                        return c.id;
                    })) + 1;

                    var newItem = {id: id, required: false, path: '', attachmentId: null, name: ''};
                    vm.currentList.push(newItem);
                    vm.isFileChanged = false;
                }
            }
        },
    },
    computed: {
        isGroupValid: function () {
            var vm = this;
            var isValid = false;
            var isDirty = false;
            var isAllValid = true;
            var list = vm.currentList;
            var validCurrentCnt = 0;
            list.forEach(function (itemObject, index) {
                validCurrentCnt = 0;
                if (itemObject.attachmentId) {
                    isValid = true;
                    isDirty = true;
                } else {
                    isValid =
                            vm.fields.$step3
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']']
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']'].valid;
                    isDirty =
                            vm.fields.$step3
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']']
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']'].dirty;
                }
                itemObject.isValid = isValid;
                itemObject.isDirty = isDirty;
                if (!isValid) {
                    isAllValid = false;
                }
            });
            list.forEach(function (itemObject, index) {
                if (itemObject.isValid == true) {
                    validCurrentCnt = validCurrentCnt + 1;
                }
                if (validCurrentCnt < vm.numRequired) {
                    isAllValid = false;
                } else {
                    isAllValid = true;
                }
            });
            if (vm.isFileChanged === true) {
                vm.validCnt = validCurrentCnt; //to trigger add new input
            }

            return isAllValid;
        },
        currentList: function () {
            var vm = this;
            var list = vm.list;
            var cnt = 0;
            list.forEach(function (itemObject, index) {
                if (itemObject.attachmentId || itemObject.isValid == true) {
                    cnt++
                }
            });
            vm.validCnt = cnt;
            list.forEach(function (itemObject, index) {
                if (index < vm.numRequired) {
                    itemObject.required = true;
                }
            });
            return list;
        }
    },
    methods: {
        onFileChange: function (item, event, name) {
            var vm = this;
            item.isLoading = true;
            vm.isFileChanged = true;
            var dataFile = event.target.files[0];
            setTimeout(function () { //need delay to wait result from computed property
                if (item.isValid == true) {
                    event.target.files = null;
                    var data = new FormData();
                    data.append(name, dataFile);
                    var url = '/installform/saveformMedia?formId=' + vm.$parent.form.id;
                    vm.$http.post(url, data).then(function (response) {
                        var jsonData = response.data;
                        if (jsonData) {
                            item.path = jsonData.path;
                            item.attachmentId = jsonData.id;
                            item.name = jsonData.name;
                            item.isLoading = false;
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }, 50);
        },
        remove: function (item, event) {
            var vm = this;
            item.path = '';

            vm.isGroupValid;

            var url = '/installform/removeformMedia?formId=' + vm.$parent.form.id + '&attachmentId=' + item.attachmentId;
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData.deleted == true) {
                    if (vm.currentList.length > vm.numRequired) {
                        var num = vm.currentList.indexOf(item);
                        vm.currentList.splice(num, 1);
                        // vm.validCnt = vm.validCnt - 1;
                    } else {
                        item.isDirty = false;
                        item.isValid = false;
                        item.attachmentId = null;
                        item.isValid = false;
                    }
                } else {
                    alert('cant delete file')
                }

            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});
var app = new Vue({
    el: ".installform",
    data: {
        facilityName:'',
        contactList: [],
        facilityAddressList: [],
        //facilityAddress: {},
        primaryContactList: [],
        primaryContact: {},
        coachingContactList: [],
        coachingContact: {},
        ITContactList: [],
        ITContact: {},
        athleticDirectorContactList: [],
        athleticDirector: {},
        sportsDirectorContactList: [],
        sportsDirectorContact: {},
        facilityManagerContactList: [],
        facilityManagerContact: {},
        cornerImageList: [],
        cornerImage: {},
        videoView: {path: '', attachmentId: null, name: ''},
        serverRoomImage: {},
        internetSpeed: {upload: null, download: null},
        needObtain: false,
        locatedOnSite: false,
        cornerList: [{id: 0, path: '', attachmentId: null, name: ''}, {id: 1, path: '', attachmentId: null, name: ''}, {id: 2, path: '', attachmentId: null, name: ''}, {id: 3, path: '', attachmentId: null, name: ''}],
        hoopsList: [{id: 0, path: '', attachmentId: null, name: ''}, {id: 1, path: '', attachmentId: null, name: ''}],
        serverRoomImgList: [{id: 0, path: '', attachmentId: null, name: ''}],
        currentStep: 1,
        stepsList: {
            step1: {is_valid: false},
            step2: {is_valid: false},
            step3: {is_valid: false}
        },
        isFomValid: false,
        blueprintList: [],
        isBlueprintRequired: false,
        isEquipmentsRequired: true,
        form: {id:null,updated_at: null},
        forms: [],
        timer: {}
    },
    components: {
        formcontactgroup: formcontactgroup,
        formuploadimage: formuploadimage,
        formcontactaddressgroup: formcontactaddressgroup
    },
    computed: {
        formValid: function () {
            var vm = this;
            var isValid = true;
            var isValidChild = true;
            var fields = vm.fields;
            var list = vm.stepsList;

            Object.keys(list).map(function (scope, index) {
                var fieldScope = vm.fields['$' + scope];
                if (fieldScope) {
                    Object.keys(fieldScope).forEach(function (key) {
                        if (fieldScope[key]) {
                            //console.log(fieldScope[key]);
                            if (fieldScope[key].valid === false || (fieldScope[key].valid === null && fieldScope[key].required === true)) {
                                //if (fieldScope[key].valid === false) {
                                isValid = false;
                            }
                        }
                    });
                }
            });

            vm.$children.map(function (child) {
                Object.keys(list).map(function (scope, index) {
                    var fieldScope = child.fields['$' + scope];
                    if (fieldScope) {
                        Object.keys(fieldScope).forEach(function (key) {
                            if (fieldScope[key]) {
                                //console.log(fieldScope[key]);
                                if (fieldScope[key].valid === false || (fieldScope[key].valid === null && fieldScope[key].required === true)) {
                                    // if (fieldScope[key].valid === false) {
                                    isValidChild = false;
                                }
                            }
                        });
                    }
                    ;
                });
            });

            if (isValid === false || isValidChild === false)
                return false;
            else
                return true;
        },
        isGroupValid: function () {
            var vm = this;
            var isValid = true;
            vm.facilityAddressList.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    isValid =
                            vm.fields.$step1
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']']
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']'].valid
                            && isValid;
                }
            });
            return isValid;
        }
    },
    watch: {
        isBlueprintRequired: function () {
            if (this.isBlueprintRequired) {
                this.blueprintList = [];
            }
        },
        needObtain: function () {
            if (this.needObtain == true) {
                this.isEquipmentsRequired = false;
                this.locatedOnSite = false;
            }
            if (this.needObtain == false && this.locatedOnSite == false) {
                this.isEquipmentsRequired = true;
            }
        },
        cornerList: function () {
            console.log('cornerList updated');
        },
        locatedOnSite: function () {
            if (this.locatedOnSite == true) {
                this.isEquipmentsRequired = false;
                this.needObtain = false;
            }
            if (this.needObtain == false && this.locatedOnSite == false) {
                this.isEquipmentsRequired = true;
            }
        },
        forms: function () {
            if (this.forms.length > 0) {
                var facilityName = this.forms[0].facility_name || '';
                if (facilityName.length > 0) {
                    $(".previous-data-popup-trigger").trigger('click');//popup when a user returns to the install form - so that they can rejoin the form they started - will show them popup with Facility Name(s) of previous entry plus START NEW FACILITY option (coming soon - waiting on final UI)
                } else {
                    this.form = this.forms[0];
                }
            }
        },
        'form.id': function () {
            this.fetchForm();
        },
        form: function () {
            this.populateForm();
        },
    },
    created: function () {
        this.initForm();
        this.fetchForms();
        this.timer = setInterval(this.autosaveForm, 20000);
    },
    methods: {
        buildApiUrl: function (url) {
            return url;
        },
        checkRadioEquipment: function () {
            //console.log(this.needObtain);
            //console.log(this.locatedOnSite);
        },
        initAddress: function () {
            return {'id': 0, 'required': true, 'list': [{address: null, city: null, state: null, zip: null}]};
        },
        initContact: function () {
            return {'id': 0, 'required': true, 'list': [{name: null, email: null, title: null, phonenumber: null}]};
        },
        initFacilityAddress: function () {
            return this.initAddress();
        },
        initPrimaryContact: function () {
            return this.initContact();
        },
        initCoachingContact: function () {
            return this.initContact();
        },
        initITContact: function () {
            return this.initContact();
        },
        initAthleticDirectorContact: function () {
            return this.initContact();
        },
        initSportsDirectorContact: function () {
            return this.initContact();
        },
        initFacilityManagerContact: function () {
            return this.initContact();
        },
        initForm: function () {
            var vm = this;
            var newItem = vm.initAddress();
            vm.facilityAddressList.push(newItem);

            //vm.primaryContact = vm.initPrimaryContact();
            var newItem = vm.initPrimaryContact();
            vm.primaryContactList.push(newItem);

            vm.coachingContact = vm.initCoachingContact();
            vm.ITContact = vm.initITContact();
            vm.athleticDirector = vm.initAthleticDirectorContact();
            vm.sportsDirectorContact = vm.initSportsDirectorContact();
            vm.facilityManagerContact = vm.initFacilityManagerContact();

            // vm.facilityAddressList.push(vm.facilityAddress);
            //vm.primaryContactList.push(vm.primaryContact);

            vm.coachingContactList.push(vm.coachingContact);
            vm.ITContactList.push(vm.ITContact);
            vm.athleticDirectorContactList.push(vm.athleticDirector);
            vm.sportsDirectorContactList.push(vm.sportsDirectorContact);
            vm.facilityManagerContactList.push(vm.facilityManagerContact);
        },
        populateForm: function ()
        {
            var vm = this;
           
            if (vm.form.updated_at) {
                var primaryContactList = Array.from(vm.form.primary_contact_list);
                if (primaryContactList.length > 0) {
                    vm.primaryContactList = primaryContactList;
                }

                vm.facilityName = vm.form.facility_name;
                vm.facilityAddressList = Array.from(vm.form.facility_address);
                vm.coachingContactList = Array.from(vm.form.coaching_contact_list);
                vm.ITContactList = Array.from(vm.form.it_contact_list);
                vm.athleticDirectorContactList = Array.from(vm.form.athletic_director_contact_list);
                vm.sportsDirectorContactList = Array.from(vm.form.sports_director_contact_list);
                vm.facilityManagerContactList = Array.from(vm.form.facility_manager_contact_list);

                var formInternetSpeed = JSON.parse(vm.form.internet_speed);
                if (formInternetSpeed.upload)
                    vm.internetSpeed.upload = formInternetSpeed.upload;
                if (formInternetSpeed.download)
                    vm.internetSpeed.download = formInternetSpeed.download;
                if (vm.form.equipment_requirements == 'needObtain')
                    vm.needObtain = 1;
                if (vm.form.equipment_requirements == 'locatedOnSite')
                    vm.locatedOnSite = 1;
                if (vm.form.gym_blueprint == 1)
                    vm.isBlueprintRequired = true;

                if (vm.form.media.VIDEO && vm.form.media.VIDEO.length > 0) {
                    vm.videoView.attachmentId = vm.form.media.VIDEO[0].id;
                    vm.videoView.name = vm.form.media.VIDEO[0].title;
                    vm.videoView.path = vm.form.media.VIDEO[0].data;
                    vm.$validator.flag('step3.installForm[video][0]', {
                        valid: true,
                        dirty: true
                    });
                }

                for (var property in vm.form.media) {
                    if (vm.form.media.hasOwnProperty(property)) {
                        var list = null;
                        if (property === 'HOOPS') {
                            var list = vm.hoopsList;
                        }
                        if (property === 'CORNER') {
                            var list = vm.cornerList;
                        }
                        if (property === 'SERVERROOMIMG') {
                            var list = vm.serverRoomImgList;
                        }
                        if (property === 'BLUEPRINT') {
                            vm.blueprintList = vm.form.media[property];
                            if (vm.blueprintList.length > 0) {
                                vm.$validator.flag('step2.installForm[blueprint][]', {
                                    valid: true,
                                    dirty: true
                                });
                            }
                        }

                        var i = 0;
                        if (vm.form.media[property] && list) {
                            vm.form.media[property].forEach(function (item) {
                                var newItem = {id: i, required: false, path: item.data, attachmentId: item.id, name: item.title, isValid: true, isDirty: true};
                                if (list[i]) {
                                    var tmp = list[i];
                                    tmp.id = i;
                                    tmp.required = newItem.required;
                                    tmp.path = item.data;
                                    tmp.attachmentId = item.id;
                                    tmp.name = item.title;
                                    tmp.isValid = item.isValid;
                                    tmp.isDirty = item.isDirty;
                                    vm.$set(list, i, tmp);
                                } else {
                                    list.push(newItem);
                                }
                                i++;
                            });
                        }
                    }
                }
                setTimeout(function () {
                    vm.validateForm('step1', false);
                }, 300);
           }
        },
        onFileChangeBlueprint: function (event, name) {
            var vm = this;
            var dataFile = event.target.files;
            event.target.files = null;
            setTimeout(function () { //need delay to wait result from computed property
                var isValid = vm.fields.$step2[name].valid;
                if (isValid == true) {
                    vm.blueprintList = [];
                    var data = new FormData();
                    for (var i = 0; i < dataFile.length; i++) {
                        data.append(name, dataFile[i]);
                    }
                    var url = '/installform/saveformMedia?formId=' + vm.form.id + '&key=blueprint';
                    vm.$http.post(url, data).then(function (response) {
                        var jsonData = response.data;
                        if (jsonData) {
                            vm.blueprintList = jsonData.data;
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }, 200);
        },
        onFileChange: function (item, event, name) {
            var vm = this;
            var stepname = 'step3.'+name; //'step3.installForm[video][0]'
            var dataFile = event.target.files[0];
            event.target.files = null;
            item.isLoading=true;
            setTimeout(function () { //need delay to wait result from computed property
                document.getElementById(name).value = "";
                item.isValid = vm.fields.$step3[name].valid;
                vm.$validator.flag(stepname, { //to prevent show valid flag while video uploading
                    valid: false,
                    dirty: true
                });
                if (item.isValid == true) {
                    var data = new FormData();
                    data.append(name, dataFile);
                    var url = '/installform/saveformMedia?formId=' + vm.form.id;
                    vm.$http.post(url, data).then(function (response) {
                        var jsonData = response.data;
                        if (jsonData) {
                            item.path = jsonData.path;
                            item.attachmentId = jsonData.id;
                            item.name = jsonData.name;
                            vm.$validator.flag(stepname, {
                                valid: true,
                                dirty: true
                            });
                            item.isLoading=false;
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }, 100);
        },
        removeVideo: function (item, name) {
            var vm = this;
            var url = '/installform/removeformMedia?formId=' + vm.form.id + '&attachmentId=' + item.attachmentId;
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData.deleted == true) {
                    item.path = '';
                    item.isDirty = null;
                    item.isValid = null;
                    item.attachmentId = null;
                    vm.$validator.flag(name, {
                        valid: false,
                        dirty: false
                    });
                } else {
                    alert('cant delete this file');
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        autosaveForm: function () {
            if (this.form.id > 0) {
                this.saveForm();
            }
        },
        fetchForms: function () {
            var vm = this;
            var url = vm.buildApiUrl('/installform/preload');
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData)
                    vm.forms = jsonData;
            }).catch(function (error) {
                console.log(error);
            });
        },
        fetchForm: function () {
            var vm = this;
            var url = vm.buildApiUrl('/installform/preload?formId=' + vm.form.id);
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData)
                    vm.form = jsonData;
            }).catch(function (error) {
                console.log(error);
            });
        },
        addAnother: function (list, type) {
            var vm = this;
            if (type == 'contact') {
                var newItem = vm.initPrimaryContact();
                //vm.primaryContactList.push(newItem);
            }
            if (type == 'address') {
                var newItem = vm.initAddress();
            }
            var id = list.length;
            newItem.id = id;
            newItem.required = false;
            list.push(newItem);
        },
        removeAnother: function (item) {
            var vm = this;
            vm.facilityAddressList.splice(vm.facilityAddressList.indexOf(item), 1);
        },
        submitForm: function () {
            var vm = this;
            var list = vm.stepsList;
            Object.keys(list).map(function (objectKey, index) {
                vm.validateForm(objectKey);
            });
            if (vm.formValid) {
                window.onbeforeunload = null;
                $.blockUI({baseZ: 2000});
                document.getElementById("install-form").submit();
            }
        },
        validateForm: function (scope, jumpToNextStep) {
            var vm = this;
            vm.stepsList[scope].is_valid = false;
            this.$validator.validateAll(scope).then(function (result) {
                if (result) {
                    vm.stepsList[scope].is_valid = true;
                    if (jumpToNextStep !== false) {
                        vm.toNextStep();
                    }
                } else {
                    if (vm.currentStep == 2) {
                        $(".various").trigger('click');
                    }
                }
            });
        },
        createForm: function () {
            var vm = this;
            /* var url = '/installform/create?formId=' + vm.form.id;
             window.onbeforeunload = null;
             window.location.replace(url);*/
            var url = vm.buildApiUrl('/installform/create');
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData)
                    vm.form = jsonData;
            }).catch(function (error) {
                console.log(error);
            });
        },        
        toPrevStep: function () {
            if (this.currentStep > 1) {
                this.currentStep -= 1;
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            }
        },
        toNextStep: function () {
            if (this.currentStep < 3) {
                this.currentStep += 1;
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                if (this.currentStep == 3) {
                    $(".court-stars-trigger").trigger('click');
                }
            }
        },
        saveForm: function () {
            var vm = this;
            console.log('save-form');
            var options = {
                success: showResponse, // post-submit callback 
                url: '/installform/saveform?formId=' + vm.form.id, // override for form's 'action' attribute 
                clearForm: false, // clear all form fields after successful submit 
                resetForm: false       // reset the form after successful submit 
            };
            var res = '';
            function showResponse(responseText, statusText, xhr, $form) {
                res = JSON.parse(responseText);
                if (res.status == 200) {
                    if (res.form.updated_at){
                        vm.form.updated_at = res.form.updated_at;
                    }
                    //vm.updateFormDetails(res.form);
                }
            }
            $('#install-form').ajaxSubmit(options);
        }
    }
});