UtilitiesInstallform = {}
UtilitiesInstallform.Common = {
    phoneMask: function (params)
    {
        params = $.extend({
        }, params);
        var pattern = '(999) 999-99999';
        $(params.el).mask(pattern);
    },
};
$(function () {
    'use strict';
    function readURL(input, targetObj, inputObj, setBackground) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var is_valid = !inputObj.parent().hasClass('has-warning');
                if (is_valid) {
                    if (setBackground !== false) {
                        targetObj.css('background-image', "url('" + e.target.result + "')");
                    }
                    targetObj.addClass('filled');
                }
            };
            return reader.readAsDataURL(input.files[0]);
        } else {
            return false;
        }
    }

    jQuery(function ($) {
        var index = 0;
        /*$('.media-upload').each(function () {
         index++;
         var targetObj = $(this);
         var labelObj = $(this).find("label");
         labelObj.attr('for', labelObj.attr('for') + '_' + index);
         var inputObj = $(this).find("input[type='file']");
         inputObj.attr('id', labelObj.attr('for'));
         if (inputObj.hasClass('file-video')) {
         inputObj.change(function () {
         if ($(this).val()) {
         var filename = $(this).val();
         filename = filename.substr(filename.lastIndexOf('\\') + 1);
         inputObj.parent().parent().find('.fileinput-filename').html(filename);
         readURL(this, targetObj, inputObj, false);
         var $source = $('#video_here');
         var files = this.files[0];
         setTimeout(function () {
         var is_valid = !inputObj.parent().hasClass('has-warning');
         if (is_valid) {
         showVideoTag($source, files);
         }
         }, 100);
         }
         });
         }
         });*/

        $(document.body).on('click', '.remove', function (event) {
            var th = $(this);
            th.next('.filled').trigger("click");
            th.removeClass('remove').addClass('remove-non-active');
            var divObj = th.parent().find('.media-upload');
            divObj.find('label').html('+Add photo');
            divObj.removeClass('cant-show-preview');
        });

        $(document.body).on('click', '.filled', function (event) {
            var inputObj = $(this).find("input[type='file']");
            inputObj.attr('disabled', false);
            inputObj.val('');
            if (inputObj.hasClass('file-video')) {
                var $source = $('#video_here');
                $source[0].src = '';
                $source.parent().hide();
            }

            $(this).removeClass('filled');
            $(this).removeClass('has-warning');
            $(this).css('background-image', 'none');
            $(this).parent().find('.fileinput-filename').text('');

            app.$validator.fields.items.forEach(function (item, index) {
                if (item.name === inputObj.attr('name')) {
                    item.flags.dirty = null;
                    item.flags.valid = null;
                }
            });

            app.$children.map(function (child) {
                child.$validator.fields.items.forEach(function (item) {
                    if (item.name === inputObj.attr('name')) {
                        item.flags.dirty = null;
                        item.flags.valid = null;
                    }
                });
            });
            event.preventDefault();
        });

        $('.multi-upload').each(function () {
            index++;
            var el = $(this);
            var inputObj = $(this).find("input[type='file']");
            inputObj.change(function () {
                el.find('.list-names').html('');
                if (this.files.length > 4) {
                    alert('Max 4 files');
                } else {
                    for (var i = 0; i <= this.files.length; i++) {
                        if (this.files.length) {
                            //console.log(this.files[i].name);
                            /*if (this.files[i]) {
                             var fileName = this.files[i].name;
                             el.find('.list-names').append('<span>' + fileName + '</span>');
                             }*/
                        }
                    }
                }
            });
        });

        $(document).ready(function () {
            $('h4').on('click', function () {
                var fa = $(this).find('.fa');
                if (fa.hasClass('fa-plus')) {
                    fa.addClass('fa-minus').removeClass('fa-plus');
                    fa.parent().next('p').show(100);
                } else if (fa.hasClass('fa-minus')) {
                    fa.addClass('fa-plus').removeClass('fa-minus');
                    fa.parent().next('p').hide(100);
                }
            });
        });

        $(document).ready(function () {
            $('.non-valid').on('click', function (event) {
                event.preventDefault();
                console.log('Form is not valid');
                setTimeout(function () {
                    var $scrollTo = $('.has-warning:first');
                    if ($scrollTo.length) {
                        console.log('scroll to try to find error');
                        $('html, body').animate({
                            scrollTop: $scrollTo.offset().top - 120
                        }, 800);
                    }
                }, 10);
            });
        });
        $(document).ready(function () {
            if ($('#install-form').length) {
                window.onbeforeunload = function () {
                    app.saveForm();
                    return 'Are you sure you want to leave? Entered data will be lost.';
                };
            }
        });
        $(document).ready(function () {
            $('.various, .def').fancybox({
                maxHeight: 700,
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                },
                afterLoad: function (current) {
                }
            });
        });
        $(document).ready(function () {
            $('.non-close-outside').fancybox({
                maxHeight: 700,
                openEffect: 'none',
                closeEffect: 'none',
                closeClick: false, // prevents closing when clicking INSIDE fancybox
                closeBtn: false,
                wrapCSS: 'fancybox-square',
                helpers: {
                    media: {},
                    overlay: {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
                },
                afterLoad: function (current) {
                }
            });
        });
        $(document).ready(function () {
            $('.fancybox-media-pdf').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                type: 'iframe',
                iframe: {
                    preload: false // fixes issue with iframe and IE
                }
            });
        });
        $(document).ready(function () {
            $('.close-popup').on('click', function (event) {

                event.preventDefault();
                $.fancybox.close();
            });

        });

        $(document).ready(function () {
            var formObj = $('#forgot-password-form');
            var options = {
                success: showResponse,
                resetForm: true        // reset the form after successful submit 
            };
            function showResponse(responseText, statusText, xhr, $form) {
                var res = JSON.parse(responseText);
                if (res.code == 200) {
                    if (res.status == 'success') {
                        formObj.find('.successMessage').show(0).delay(4000).hide(0);
                        setTimeout('$.fancybox.close();', 4000);
                    } else {
                        formObj.find('.errors-box').html(res.message);
                    }
                }
            }

            formObj.ajaxForm(options);
        });

        $(document).ready(function () {
            $('.fancybox-media-court').fancybox({
                maxHeight: 700,
                maxWidth: 700,
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                },
                afterShow: function () {
                    $('.list-tooltips img').hide();
                    $('.court-stars div').hover(function (e) {
                        var num = $(this).index();
                        console.log('num: ' + num);
                        var $img = $('.list-tooltips img').eq(num);
                        $img.stop(1, 1).fadeIn();

                        // var pageX = e.pageX < 800 ? e.pageX : 700;
                        var pageX = e.pageX;
                        if (num === 7 || num === 6 || num === 9) {
                            pageX = e.pageX + 150;
                        }
                        if (num === 10 || num === 8) {
                            pageX = e.pageX - 150;
                        }
                        var top = e.pageY - $img.outerHeight() - 10;
                        var left = pageX - ($img.outerWidth() / 2);
                        $img.offset({
                            top: top,
                            left: left
                        });
                    }).mouseleave(function () {
                        $('.list-tooltips img').hide();
                        //$img.fadeOut();
                    });

                    $('.close-popup').on('click', function (event) {
                        event.preventDefault();
                        $.fancybox.close();
                    });
                },
                afterLoad: function (current) {
                }
            });
        });
    });
});
