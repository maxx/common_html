$(function () {
    "use strict";

    var i = new WOW({
        offset: 120,
        boxClass: "wow",
        animateClass: "animated",
        live: !0
    });
    i.init();
    $(document).ready(function () {
        $('.fancybox-media').fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                media: {}
            }
        });

        $('.container.skewed-content.top video').hover(function toggleControls() {
            this.attr("controls", false);
        });
        $('video').attr("controls", false);
    });
    
});


