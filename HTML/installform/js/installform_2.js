function lg(e){
    return console.log(e);
}
Vue.prototype.$http = axios;

Vue.use(VeeValidate);
Vue.directive('checkbox-uploaded', function (el, binding) {
    var elObj = $(el);
    elObj.change(function () {
        var isChecked = elObj.is(':checked');
        var rowParent = elObj.parents('.multi-upload');
        var inputObj = rowParent.find("input[type='file']");

        if (isChecked) {
            inputObj.attr('disabled', true);
            inputObj.val('');
            rowParent.find('.list-names').text('');
            app.$validator.fields.items.forEach(function (item) {
                if (item.name === elObj.data('target')) {
                    item.flags.valid = true;
                }
            });
        } else {
            if (app) {
                app.$validator.fields.items.forEach(function (item) {
                    if (item.name === elObj.data('target')) {
                        item.flags.valid = null;
                    }
                });
            }
            //inputObj.attr('disabled', false);
        }
    });
});

/*function showVideoTag(source, files) {
    source[0].src = URL.createObjectURL(files);
    source.parent().show();
    source.parent()[0].load();
}
function readURL(input, targetObj, inputObj, setBackground) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var label = targetObj.find('label');
            label.css('background-image', "url('/img/spinner.gif')");
            label.css('background-size', "100%");
            //console.log(e);
            setTimeout(function () {
                var is_valid = !inputObj.parent().hasClass('has-warning');
                if (is_valid) {
                    if (setBackground !== false) {
                        var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
                        if (e.total > 2212050 && isIE11) {
                            targetObj.addClass('cant-show-preview');
                            label.html("Image uploaded but \n\
                                            can't show preview");
                        } else {
                            targetObj.css('background-image', "url('" + e.target.result + "')");
                        }
                        label.css('background-image', "");
                    }
                    targetObj.addClass('filled');
                    targetObj.parent().find('.remove-non-active').removeClass('remove-non-active').addClass('remove');
                }
            }, 110);
        };
        return reader.readAsDataURL(input.files[0]);
    } else {
        return false;
    }
}*/

Vue.directive('upload-image', function (el, binding) {
    var elObj = $(el);
    var targetObj = elObj;
    var inputObj = elObj.find("input[type='file']");
    var labelObj = elObj.find("label");
    labelObj.attr('for', inputObj.attr('name'));
    inputObj.attr('id', labelObj.attr('for'));
    inputObj.change(function () {
        /*if ($(this).val()) {
            var filename = $(this).val();
            filename = filename.substr(filename.lastIndexOf('\\') + 1);
            inputObj.parent().parent().find('.fileinput-filename').html(filename);
            readURL(this, targetObj, inputObj);
        }*/
    });
});

var formcontactgroup = Vue.extend({
    template: '#template-contact-form-group',
    props: {list: {type: Array}, type: {type: String}, isRequired: {type: Boolean, default: true}, elName: {type: String}, indexId: {type: Number}},
    inject: {
        $validator: '$validator'
    },
    data: function () {
        return {
            itemList: this.list,
            itemV: null,
        }
    },
    methods: {
        addAnother: function (list) {
            this.$parent.addAnother(list, this.type);
        },
        removeAnother: function (item, list) {
            list.splice(list.indexOf(item), 1);
        },
        updateValue: function (item) {
            var vm = this;
            vm.itemV = item;
        },
        check: function () {
            var vm = this;
            var hasOneDirty = false;
            vm.list.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    if (itemObject[propertyName]) {
                        hasOneDirty = true;
                    }
                }
            });
            return hasOneDirty;
        },
    },
    computed: {
        isRequiredDependenceOnOtherFilledFields: function () {
            var vm = this;
            var hasD = vm.itemV;
            var hasOneDirty = vm.check();
            if (hasOneDirty || vm.isRequired) {
                return true;
            } else {
                return false;
            }
        },
        isGroupValid: function () {
            var vm = this;
            var isValid = true;
            var hasOneDirty = vm.check();
            vm.list.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    isValid =
                            vm.fields.$step1
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']']
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']'].valid
                            && isValid;
                }
            });

            //If a section is not filled out, but not required, no green arrow should show up
            if (isValid && hasOneDirty)
                return isValid;
            else
                return null;
        }
    }
});


var formcontactaddressgroup = Vue.extend({
    template: '#template-contact-address-form-group',
    props: {list: {type: Array}, type: {type: String}, isRequired: {type: Boolean, default: true}, elName: {type: String}, indexId: {type: Number}},
    inject: {
        $validator: '$validator'
    },
    data: function () {
        return {
            itemList: this.list,
            itemV: null
        }
    },
    methods: {
        addAnother: function (list) {
            this.$parent.addAnother(list, this.type);
        },
        removeAnother: function (item, list) {
            list.splice(list.indexOf(item), 1);
        },
        updateValue: function (item) {
            var vm = this;
            vm.itemV = item;
        },
        check: function () {
            var vm = this;
            var hasOneDirty = false;
            vm.list.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    if (itemObject[propertyName]) {
                        hasOneDirty = true;
                    }
                }
            });
            return hasOneDirty;
        },
    },
    computed: {
        isRequiredDependenceOnOtherFilledFields: function () {
            var vm = this;
            var hasD = vm.itemV;
            var hasOneDirty = vm.check();
            if (hasOneDirty || vm.isRequired) {
                return true;
            } else {
                return false;
            }
        },
        isGroupValid: function () {
            var vm = this;
            var isValid = true;
            vm.list.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    isValid =
                            vm.fields.$step1
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']']
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']'].valid
                            && isValid;
                }
            });
            return isValid;
        }
    }
});

var formuploadimage = Vue.extend({
    template: '#template-form-upload-image',
    props: {list: {type: Array}, item: {}, elName: {type: String}, numRequired: {type: Number, default: 1}},
    inject: {
        $validator: '$validator'
    },
    data: function () {
        return {
            validCnt: 0,
            isFileChanged: false,
           // currentList: this.list,
            limitUpload: 11 // 12 inputs per group
        }
    },
    watch: {
        validCnt: function () {
            var vm = this;
            if (vm.validCnt >= vm.numRequired) {
                if (vm.currentList.length === vm.validCnt && vm.currentList.length <= vm.limitUpload) {
                    var id = Math.max.apply(Math, vm.currentList.map(function (c) {
                        return c.id;
                    })) + 1;

                    var newItem = {id: id, required: false, path:'', attachmentId:null, name:''};
                    vm.currentList.push(newItem);
                    vm.isFileChanged = false;
                }
            }
        },
    },
    computed: {
        isGroupValid: function () {
            var vm = this;
            var isValid = false;
            var isDirty = false;
            var isAllValid = true;
            var list = vm.currentList;
            var validCurrentCnt = 0;
            list.forEach(function (itemObject, index) {
                validCurrentCnt = 0;
                if (itemObject.attachmentId) {
                    isValid = true;
                    isDirty = true;
                } else {
                    isValid =
                            vm.fields.$step3
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']']
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']'].valid;
                    isDirty =
                            vm.fields.$step3
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']']
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']'].dirty;
                }
                itemObject.isValid = isValid;
                itemObject.isDirty = isDirty;
                if (!isValid) {
                    isAllValid = false;
                }
            });
            list.forEach(function (itemObject, index) {
                if (itemObject.isValid == true) {
                    validCurrentCnt = validCurrentCnt + 1;
                }
                if (validCurrentCnt < vm.numRequired) {
                    isAllValid = false;
                } else {
                    isAllValid = true;
                }
            });
            if (vm.isFileChanged === true) {
                vm.validCnt = validCurrentCnt; //to trigger add new input
            }

            return isAllValid;
        },
        currentList:function(){
            var vm = this;
            var list = vm.list;
            var cnt = 0;
             list.forEach(function (itemObject, index) {
                if (itemObject.attachmentId || itemObject.isValid==true){
                   cnt++
                }
            });
            vm.validCnt= cnt;
            return list;
        }
    },
    methods: {
        onFileChange: function (item, event, name) {
            var vm = this;
            vm.isFileChanged = true;
            var dataFile = event.target.files[0];
            event.target.files[0] = null;
            setTimeout(function () { //need delay to wait result from computed property
                lg(name);
                if (item.isValid == true) {
                    var data = new FormData();
                    data.append(name, dataFile);                    
                    var url = '/installform/saveformMedia?formId=' + vm.$parent.form.id;
                    vm.$http.post(url, data).then(function (response) {
                        var jsonData = response.data;
                        if (jsonData){
                            item.path = jsonData.path;
                            item.attachmentId = jsonData.id;  
                            item.name = jsonData.name;
                        }
                       // event.target.files[0] = null;
                         console.log(jsonData);
                    }).catch(function (error) {
                        console.log(error);
                    });
                } else {

                }
            }, 100);
        },
        remove: function (item, event) {
            var vm = this;
            item.path = '';
            item.isDirty = null;
            item.isValid = null;
            vm.isGroupValid;
            var url = '/installform/removeformMedia?formId=' + vm.$parent.form.id + '&attachmentId=' + item.attachmentId;
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData.deleted==true) {
                    if (vm.currentList.length > vm.numRequired) {
                        var num = vm.currentList.indexOf(item);
                        vm.currentList.splice(num, 1);
                        vm.validCnt = vm.validCnt - 1;
                    }
                    vm.currentList.forEach(function (itemObject, index) {
                        if (index < vm.numRequired) {
                            itemObject.required = true;
                        }
                    });
                } else {
                    alert('cant delete file')
                }

            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});
var app = new Vue({
    el: ".installform",
    data: {
        contactList: [],
        facilityAddressList: [],
        //facilityAddress: {},
        primaryContactList: [],
        primaryContact: {},
        coachingContactList: [],
        coachingContact: {},
        ITContactList: [],
        ITContact: {},
        athleticDirectorContactList: [],
        athleticDirector: {},
        sportsDirectorContactList: [],
        sportsDirectorContact: {},
        facilityManagerContactList: [],
        facilityManagerContact: {},
        cornerImageList: [],
        cornerImage: {},
        videoView: {path:'', attachmentId:null, name:''},
        serverRoomImage: {},
        internetSpeed: {upload: null, download: null},
        needObtain: false,
        locatedOnSite: false,
        cornerList: [{id: 0, path:'', attachmentId:null, name:''}, {id: 1, path:'', attachmentId:null, name:''}, {id: 2, path:'', attachmentId:null, name:''}, {id: 3, path:'', attachmentId:null, name:''}],
        hoopsList: [{id: 0, path:'', attachmentId:null, name:''}, {id: 1, path:'', attachmentId:null, name:''}],
        serverRoomImgList: [{id: 0, path:'', attachmentId:null, name:''}],
        currentStep: 1,
        stepsList: {
            step1: {is_valid: false},
            step2: {is_valid: false},
            step3: {is_valid: false}
        },
        isFomValid: false,
        isBlueprintRequired: false,
        isEquipmentsRequired: true,
        
        form:{updated_at:null},
        timer:{}
    },
    components: {
        formcontactgroup: formcontactgroup,
        formuploadimage: formuploadimage,
        formcontactaddressgroup: formcontactaddressgroup
    },
    computed: {
        formValid: function () {
            var vm = this;
            var isValid = true;
            var isValidChild = true;
            var fields = vm.fields;
            var list = vm.stepsList;

            Object.keys(list).map(function (scope, index) {
                var fieldScope = vm.fields['$' + scope];
                if (fieldScope) {
                    Object.keys(fieldScope).forEach(function (key) {
                        if (fieldScope[key]) {
                            //console.log(fieldScope[key]);
                            if (fieldScope[key].valid === false || (fieldScope[key].valid === null && fieldScope[key].required === true)) {
                                //if (fieldScope[key].valid === false) {
                                isValid = false;
                            }
                        }
                    });
                }
            });

            vm.$children.map(function (child) {
                Object.keys(list).map(function (scope, index) {
                    var fieldScope = child.fields['$' + scope];
                    if (fieldScope) {
                        Object.keys(fieldScope).forEach(function (key) {
                            if (fieldScope[key]) {
                                //console.log(fieldScope[key]);
                                if (fieldScope[key].valid === false || (fieldScope[key].valid === null && fieldScope[key].required === true)) {
                                    // if (fieldScope[key].valid === false) {
                                    isValidChild = false;
                                }
                            }
                        });
                    }
                    ;
                });
            });

            if (isValid === false || isValidChild === false)
                return false;
            else
                return true;
        },
        isGroupValid: function () {
            var vm = this;
            var isValid = true;
            vm.facilityAddressList.forEach(function (itemObject, index) {
                for (var propertyName in itemObject) {
                    isValid =
                            vm.fields.$step1
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']']
                            && vm.fields.$step1['installForm[' + vm.elName + '][' + vm.indexId + '][' + propertyName + ']'].valid
                            && isValid;
                }
            });
            return isValid;
        }
    },
    watch: {
        needObtain: function () {
            if (this.needObtain == true) {
                this.isEquipmentsRequired = false;
                this.locatedOnSite = false;
            }
            if (this.needObtain == false && this.locatedOnSite == false) {
                this.isEquipmentsRequired = true;
            }
        },
        cornerList: function () {
            console.log('cornerList updated');
        },
        locatedOnSite: function () {
            if (this.locatedOnSite == true) {
                this.isEquipmentsRequired = false;
                this.needObtain = false;
            }
            if (this.needObtain == false && this.locatedOnSite == false) {
                this.isEquipmentsRequired = true;
            }
        },
        form:function(){
            this.populateForm();
        },
    },
    created: function () {
        this.initForm();
        this.fetchForm();

    },
    methods: {
        buildApiUrl: function (url) {
            return url;
        },        
        checkRadioEquipment: function () {
            console.log(this.needObtain);
            console.log(this.locatedOnSite);
        },
        initAddress: function () {
            return {'id': 0, 'required': true, 'list': [{address: null, city: null, state: null, zip: null}]};
        },
        initContact: function () {
            return {'id': 0, 'required': true, 'list': [{name: null, email: null, title: null, phonenumber: null}]};
        },
        initFacilityAddress: function () {
            return this.initAddress();
        },
        initPrimaryContact: function () {
            return this.initContact();
        },
        initCoachingContact: function () {
            return this.initContact();
        },
        initITContact: function () {
            return this.initContact();
        },
        initAthleticDirectorContact: function () {
            return this.initContact();
        },
        initSportsDirectorContact: function () {
            return this.initContact();
        },
        initFacilityManagerContact: function () {
            return this.initContact();
        },
        initForm: function () {
            var vm = this;
            var newItem = vm.initAddress();
            vm.facilityAddressList.push(newItem);


            //vm.primaryContact = vm.initPrimaryContact();
            var newItem = vm.initPrimaryContact();
            vm.primaryContactList.push(newItem);

            vm.coachingContact = vm.initCoachingContact();
            vm.ITContact = vm.initITContact();
            vm.athleticDirector = vm.initAthleticDirectorContact();
            vm.sportsDirectorContact = vm.initSportsDirectorContact();
            vm.facilityManagerContact = vm.initFacilityManagerContact();

            // vm.facilityAddressList.push(vm.facilityAddress);
            //vm.primaryContactList.push(vm.primaryContact);

            vm.coachingContactList.push(vm.coachingContact);
            vm.ITContactList.push(vm.ITContact);
            vm.athleticDirectorContactList.push(vm.athleticDirector);
            vm.sportsDirectorContactList.push(vm.sportsDirectorContact);
            vm.facilityManagerContactList.push(vm.facilityManagerContact);
        },
        
        populateForm: function ()
        {
            var vm = this;
            //vm.cornerList = [];
            if (vm.form.updated_at) {
                vm.primaryContactList = Array.from(vm.form.primary_contact_list);
                vm.facilityAddressList = Array.from(vm.form.facility_address);
                vm.coachingContactList = Array.from(vm.form.coaching_contact_list);
                vm.ITContactList = Array.from(vm.form.it_contact_list);
                vm.athleticDirectorContactList = Array.from(vm.form.athletic_director_contact_list);
                vm.sportsDirectorContactList = Array.from(vm.form.sports_director_contact_list);
                vm.facilityManagerContactList = Array.from(vm.form.facility_manager_contact_list);
                var formInternetSpeed =JSON.parse(vm.form.internet_speed);
                if (formInternetSpeed.upload)
                    vm.internetSpeed.upload = formInternetSpeed.upload;
                if (formInternetSpeed.download)
                    vm.internetSpeed.download = formInternetSpeed.download;
                if (vm.form.equipment_requirements=='needObtain')
                    vm.needObtain = 1;
                if (vm.form.equipment_requirements=='locatedOnSite')
                    vm.locatedOnSite = 1;
                if (vm.form.gym_blueprint==1)
                    vm.isBlueprintRequired = true;
                
                if(vm.form.media.VIDEO && vm.form.media.VIDEO.length>0){
                    vm.videoView.attachmentId = vm.form.media.VIDEO[0].id;
                    vm.videoView.name = vm.form.media.VIDEO[0].title;
                    vm.videoView.path = vm.form.media.VIDEO[0].data;
                    vm.$validator.flag('step3.installForm[video][0]', {
                        valid: true,
                        dirty: true
                    });
                }

                for (var property in vm.form.media) {
                    if (vm.form.media.hasOwnProperty(property)) {
                        var list = null;
                        if (property === 'HOOPS') {
                            var list = vm.hoopsList;
                        }
                        if (property === 'CORNER') {
                            var list = vm.cornerList;
                        }
                        if (property === 'SERVERROOMIMG') {
                            var list = vm.serverRoomImgList;
                        }
                        var i = 0;
                        if (vm.form.media[property] && list) {
                            vm.form.media[property].forEach(function (item) {
                                var newItem = {id: i, required: false, path: item.data, attachmentId: item.id, name: item.title, isValid: true, isDirty: true};
                                if (list[i]) {
                                    var tmp = list[i];
                                    tmp.id = i;
                                    tmp.required = newItem.required;
                                    tmp.path = item.data;
                                    tmp.attachmentId = item.id;
                                    tmp.name = item.title;
                                    tmp.isValid = item.isValid;
                                    tmp.isDirty = item.isDirty;
                                    vm.$set(list, i, tmp);
                                } else {
                                    list.push(newItem);
                                }
                                i++;
                            });
                        }
                    }
                }
            } 
            
            //vm.timer = setInterval(vm.autosaveForm, 60000);
            vm.timer = setInterval(vm.autosaveForm, 20000);
        },
        onFileChange: function (item, event, name) {
            var vm = this;
            //vm.isFileChanged = true;
            var dataFile = event.target.files[0];
            event.target.files= null;
            setTimeout(function () { //need delay to wait result from computed property
                item.isValid = vm.fields.$step3['installForm[video][0]'].valid;
                if (item.isValid == true) {
                    var data = new FormData();
                    data.append(name, dataFile);                    
                    var url = '/installform/saveformMedia?formId=' + vm.form.id;
                    vm.$http.post(url, data).then(function (response) {
                        var jsonData = response.data;
                        if (jsonData){
                            item.path = jsonData.path;
                            item.attachmentId = jsonData.id;  
                            item.name = jsonData.name;
                            document.getElementById("installForm[video][0]").value = "";
                             vm.$validator.flag('step3.installForm[video][0]', {
                        valid: true,
                        dirty: true
                    });
                        }
                         console.log(jsonData);
                    }).catch(function (error) {
                        console.log(error);
                    });
                } else {

                }
            }, 100);
        }, 
        removeVideo: function (item, name) {
            var vm = this;
            var url = '/installform/removeformMedia?formId=' + vm.form.id + '&attachmentId=' + item.attachmentId;
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData.deleted == true) {
                    item.path = '';
                    item.isDirty = null;
                    item.isValid = null;
                    item.attachmentId = null;
                    vm.$validator.flag(name, {
                        valid: false,
                        dirty: false
                    });
                } else {
                    alert('cant delete this file');
                }
            }).catch(function (error) {
                console.log(error);
            });
        },        
        autosaveForm: function () {
              this.saveForm();
        },
        fetchForm: function (){
        var vm = this;
            var url = vm.buildApiUrl('/customers/preload');
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData)
                    vm.form = jsonData;
            }).catch(function (error) {
                console.log(error);
            });
        },
        addAnother: function (list, type) {
            var vm = this;
            if (type == 'contact') {
                var newItem = vm.initPrimaryContact();
                //vm.primaryContactList.push(newItem);
            }
            if (type == 'address') {
                var newItem = vm.initAddress();
            }
            var id = list.length;
            newItem.id = id;
            newItem.required = false;
            list.push(newItem);
        },
        removeAnother: function (item) {
            var vm = this;
            vm.facilityAddressList.splice(vm.facilityAddressList.indexOf(item), 1);
        },
        submitForm: function () {
            var vm = this;
            var list = vm.stepsList;
            Object.keys(list).map(function (objectKey, index) {
                vm.validateForm(objectKey);
            });
            if (vm.formValid) {
                window.onbeforeunload = null;
                $.blockUI({baseZ: 2000});
                document.getElementById("install-form").submit();
            }
        },
        validateForm: function (scope) {
            var vm = this;
            vm.stepsList[scope].is_valid = false;
            this.$validator.validateAll(scope).then(function (result) {
                if (result) {
                    vm.stepsList[scope].is_valid = true;
                    vm.toNextStep();
                } else {
                    if (vm.currentStep == 2) {
                        $(".various").trigger('click');
                    }
                }
            });
        },
        /*onFileChangeServerRoomImage: function (e) {
            var vm = this;
            this.onFileChange(e, vm.serverRoomImage);
        },
        /*onFileChange: function (e, model) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
                return;
            this.createImage(files[0], model);
        },*/
        /*createImage: function (file, model) {
            model = new Image();
            var reader = new FileReader();
            var vm = this;
            reader.onload = function (e) {
                vm.model = e.target.result;
            };

            reader.readAsDataURL(file);
        },
        removeImage: function (e) {
            this.image = '';
        },*/
        toPrevStep: function () {
            if (this.currentStep > 1) {
                this.currentStep -= 1;
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            }
        },
        toNextStep: function () {
            if (this.currentStep < 3) {
                this.currentStep += 1;
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                if (this.currentStep == 3) {
                    $(".court-stars-trigger").trigger('click');
                }
            }
        },
        saveForm: function () {
            var vm = this;
            console.log('save-form');
            //console.log(vm.form);

            var options = {
                success: showResponse, // post-submit callback 
                url: '/installform/saveform?formId=' + vm.form.id, // override for form's 'action' attribute 
                clearForm: false, // clear all form fields after successful submit 
                resetForm: false       // reset the form after successful submit 
            };
            var res = '';
            function showResponse(responseText, statusText, xhr, $form) {
                res = JSON.parse(responseText);      
                if (res.status == 200) {
                    //vm.updateFormDetails(res.form);
                }
            }
            
            $('#install-form').ajaxSubmit(options);


            /*var url = vm.buildApiUrl('/installform/saveform?formId='+vm.form.id);
             $.post(url, $("#install-form").serialize(), function (data) {
             console.log(data);
             });*/
        },
      /*   updateFormDetails: function (form) {
            var vm = this;
            vm.form.updated_at = form.updated_at;

            var i = 0;
            var list = $('.cornerlist');
            form.media.CORNER.forEach(function (item) {
                var obj = list.find('.media-upload').eq(i);
                updateMedia(obj, item);
                i++;
            });
            
            var list = $('.hooplist');
            var i = 0;
            if (form.media.HOOPS) {
                form.media.HOOPS.forEach(function (item) {
                    var obj = list.find('.media-upload').eq(i);
                    updateMedia(obj, item);
                    i++;
                });
            }
            //console.log(form.media.HOOPS.length);
            //updateList(vm.hoopsList);
            
    
            //updateList(vm.cornerList);            
            
            var i = 0;
            var list = $('.serverroomlist');
            form.media.CORNER.forEach(function (item) {
                var obj = list.find('.media-upload').eq(i);
                updateMedia(obj, item);
                i++;
            });   
            //updateList(vm.serverRoomImgList);            
            
            function updateList(list) {
                if (list) {
                    var id = Math.max.apply(Math, list.map(function (c) {
                        return c.id;
                    })) + 1;
                    list.push({id: id});
                }
            }
            
            function updateMedia(obj, item) {
                  if (!obj.hasClass('filled')) {
                    obj.parent().find('.remove-non-active').removeClass('remove-non-active').addClass('remove');
                    obj.addClass('filled');
                    obj.css('background-image', "url('" + item.data + "')");
                }
                obj.attr('data-attachment-id', item.id);
            }            
         }*/
    },
});

UtilitiesInstallform = {}
UtilitiesInstallform.Common = {
    phoneMask: function (params)
    {
        params = $.extend({
        }, params);
        var pattern = '(999) 999-99999';
        $(params.el).mask(pattern);
    },
};

$(function () {
    'use strict';
    function readURL(input, targetObj, inputObj, setBackground) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var is_valid = !inputObj.parent().hasClass('has-warning');
                if (is_valid) {
                    if (setBackground !== false) {
                        targetObj.css('background-image', "url('" + e.target.result + "')");
                    }
                    targetObj.addClass('filled');
                }
            };
            return reader.readAsDataURL(input.files[0]);
        } else {
            return false;
        }
    }

    jQuery(function ($) {
        var index = 0;
       /* $('.media-upload').each(function () {
            index++;
            var targetObj = $(this);
            var labelObj = $(this).find("label");
            labelObj.attr('for', labelObj.attr('for') + '_' + index);
            var inputObj = $(this).find("input[type='file']");
            inputObj.attr('id', labelObj.attr('for'));
            if (inputObj.hasClass('file-video')) {
                inputObj.change(function () {
                    if ($(this).val()) {
                        var filename = $(this).val();
                        filename = filename.substr(filename.lastIndexOf('\\') + 1);
                        inputObj.parent().parent().find('.fileinput-filename').html(filename);
                        readURL(this, targetObj, inputObj, false);
                        var $source = $('#video_here');
                        var files = this.files[0];
                        setTimeout(function () {
                            var is_valid = !inputObj.parent().hasClass('has-warning');
                            if (is_valid) {
                                showVideoTag($source, files);
                            }
                        }, 100);
                    }
                });
            }
        });
*/
        $(document.body).on('click', '.remove', function (event) {
            var th = $(this);
            th.next('.filled').trigger("click");
            th.removeClass('remove').addClass('remove-non-active');
            var divObj = th.parent().find('.media-upload');
            divObj.find('label').html('+Add photo');
            divObj.removeClass('cant-show-preview');
        });

     /*   $(document.body).on('click', '.filled', function (event) {
            var inputObj = $(this).find("input[type='file']");
            inputObj.attr('disabled', false);
            inputObj.val('');
            if (inputObj.hasClass('file-video')) {
                var $source = $('#video_here');
                $source[0].src = '';
                $source.parent().hide();
            }

            $(this).removeClass('filled');
            $(this).removeClass('has-warning');
            $(this).css('background-image', 'none');
            $(this).parent().find('.fileinput-filename').text('');

            app.$validator.fields.items.forEach(function (item, index) {
                if (item.name === inputObj.attr('name')) {
                    item.flags.dirty = null;
                    item.flags.valid = null;
                }
            });

            app.$children.map(function (child) {
                child.$validator.fields.items.forEach(function (item) {
                    if (item.name === inputObj.attr('name')) {
                        item.flags.dirty = null;
                        item.flags.valid = null;
                    }
                });
            });
            event.preventDefault();
        });*/

        $('.multi-upload').each(function () {
            index++;
            var el = $(this);
            var inputObj = $(this).find("input[type='file']");
            inputObj.change(function () {
                el.find('.list-names').html('');
                if (this.files.length > 4) {
                    alert('Max 4 files');
                } else {
                    for (var i = 0; i <= this.files.length; i++) {
                        if (this.files.length) {
                            //console.log(this.files[i].name);
                            if (this.files[i]) {
                                var fileName = this.files[i].name;
                                el.find('.list-names').append('<span>' + fileName + '</span>');
                            }
                        }
                    }
                }
            });
        });

        $(document).ready(function () {
            $('h4 .fa').on('click', function () {
                if ($(this).hasClass('fa-plus')) {
                    $(this).addClass('fa-minus').removeClass('fa-plus');
                    $(this).parent().next('p').show(100);
                } else if ($(this).hasClass('fa-minus')) {
                    $(this).addClass('fa-plus').removeClass('fa-minus');
                    $(this).parent().next('p').hide(100);
                }
            });
        });

        $(document).ready(function () {
            $('.non-valid').on('click', function (event) {
                event.preventDefault();
                console.log('Form is not valid');
                setTimeout(function () {
                    var $scrollTo = $('.has-warning:first');
                    if ($scrollTo.length) {
                        console.log('scroll to try to find error');
                        $('html, body').animate({
                            scrollTop: $scrollTo.offset().top - 120
                        }, 800);
                    }
                }, 10);
            });
        });
        $(document).ready(function () {
            if ($('#install-form').length) {
                window.onbeforeunload = function () {
                    app.saveForm();
                    return 'Are you sure you want to leave? Entered data will be lost.';
                };
            }
        });
        $(document).ready(function () {
            $('.various').fancybox({
                maxHeight: 700,
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                },
                afterLoad: function (current) {
                }
            });
        });
        $(document).ready(function () {
            $('.fancybox-media-pdf').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                type: 'iframe',
                iframe: {
                    preload: false // fixes issue with iframe and IE
                }
            });
        });

        $(document).ready(function () {
            $('.fancybox-media-court').fancybox({
                maxHeight: 700,
                maxWidth: 700,
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                },
                afterShow: function () {
                    $('.list-tooltips img').hide();
                    $('.court-stars div').hover(function (e) {
                        var num = $(this).index();
                        console.log('num: ' + num);
                        var $img = $('.list-tooltips img').eq(num);
                        $img.stop(1, 1).fadeIn();

                        // var pageX = e.pageX < 800 ? e.pageX : 700;
                        var pageX = e.pageX;
                        if (num === 7 || num === 6 || num === 9) {
                            pageX = e.pageX + 150;
                        }
                        if (num === 10 || num === 8) {
                            pageX = e.pageX - 150;
                        }
                        var top = e.pageY - $img.outerHeight() - 10;
                        var left = pageX - ($img.outerWidth() / 2);
                        $img.offset({
                            top: top,
                            left: left
                        });
                    }).mouseleave(function () {
                        $('.list-tooltips img').hide();
                        //$img.fadeOut();
                    });

                    $('.close-popup').on('click', function (event) {
                        event.preventDefault();
                        $.fancybox.close();
                    });
                },
                afterLoad: function (current) {
                }
            });
        });
    });
});
