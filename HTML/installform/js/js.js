
$(function () {
    var prone_tabs = $('.phone_tabs li');

    prone_tabs.hover(function () {
        var that = $(this);
        prone_tabs.removeClass('active');
        that.addClass('active');

        image = '#' + that.data('tab-img');
        $('.image_tabs img').removeClass('active');
        $(image).addClass('active');
    });

    var x = $('.article .row-b').outerHeight();
    $(".article .menu").stick_in_parent({offset_top: x + 45});

    var prone_tabs_span = $('.home_controlls span');

    prone_tabs_span.click(function () {
        var that = $(this);
        prone_tabs_span.removeClass('active');
        that.addClass('active');

        slides = that.attr('id');

        $('.image_tabs img').removeClass('active');
        $('.home_tabs_text div').removeClass('active');
        $('*[data-tab-img-home=' + "\"" + slides + "\"" + ']').addClass('active');
    });

    function turnOnVideo() {
        if ($('#head-video').length) {
            $('#head-video').get(0).play();
        }
        if ($('#head-video2').length) {
            $('#head-video2').get(0).play();
        }
        if ($('#head-video-hm').length) {
            $('#head-video-hm').get(0).play();
        }
    }
    function turnOnTestimonials() {
        $('.expertsSays').owlCarousel({
            autoPlay: 7000,
            dots: false,
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: false,
            items: 2,
            navigationText: ['<a class="esn-left">&#9658;</a>', '<a class="esn-right">&#9658;</a>']

        });
        $('#expertSays').owlCarousel({
            autoPlay: 7000,
            dots: false,
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            navigationText: ['<a class="esn-left">&#9658;</a>', '<a class="esn-right">&#9658;</a>']

        });
    }
    function turnOnLogos() {
        $('.carousel').owlCarousel({
            stopOnHover: true,
            slideSpeed: 1000,
            rewindSpeed: 0,
            autoPlay: 2000,
            navigation: false,
            pagination: false,
            responsive: true
        });
    }

    $(document).ready(function () {
        turnOnVideo();
        turnOnTestimonials();
        turnOnLogos();
    });


    $(document).ready(function () {
        $('.video-wrapper').on('click', function (event) {
            $(this).next('iframe')[0].src += "&autoplay=1";
            event.preventDefault();
        });
    });
    $(document).ready(function () {
        $('#SubscribeForm_state_code').on('change', function (event) {
            if ($(this).val() === '') {
                $(this).css('color', '#757575');
            } else {
                $(this).css('color', '#FFF');
            }
        });
    });

    function searchStringInArray(str, strArray) {
        return strArray.filter(function (name) {
            if (name.search(str) !== -1) {
                return name;
            }
        });
    }
    $(document).ready(function () {
        var ar = [];
        $('.faq-list .panel-body h5').each(function () {
            var string = '<h5>' + $(this).text().trim().toLowerCase() + '</h5><p>' + $(this).next('p').text().trim().toLowerCase() + $(this).next('p').next('ul').html();
            ar.push(string);
        });

        $('#search-answers').on('keyup', function () {
            var txt = $(this).val().toLowerCase();
            if (txt.length > 0) {
                var res = searchStringInArray(txt, ar);
                if (res.length > 0) {
                    $('.search-result').show().html(res);
                } else {
                    $('.search-result').hide().html('');
                }
                ;
            } else {
                $('.search-result').hide().html('');
            }
        });
    });

    $('.get-complete-botside .gcb-menu a, .phone_tabs_mob_nav.team a').click(function (e) {
        e.preventDefault();
        var obj = $(this);
        changeImgSrs(obj);
    });

    $('.get-complete-botside .gcb-menu a').hover(function () {
        var obj = $(this);
        changeImgSrs(obj);
    });

    function changeImgSrs(obj) {

        var index = obj.index();
        $('.target-tabs img').hide();
        $('.target-tabs img:eq(' + index + ')').show();
        $('.get-complete-botside .gcb-menu  a').removeClass('selected');
        obj.addClass('selected');

        return false;
    }

    $('.phone_tabs_mob_nav a').on('click', function () {
        $('.phone_tabs_mob_nav a').removeClass('active');
        $(this).addClass('active');
        var temp = $(this).attr('data-tab-id');
        $('.image_tabs img').removeClass('active');
        $('#' + temp).addClass('active');
        $('.phone_tabs li').removeClass('active');
        console.log(temp);
        $('li[data-tab-img="' + temp + '"]').addClass('active');

    });
    $(document).ready(function () {
        $('.fancybox-media').fancybox({
            maxHeight: 700,
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                media: {}
            },
            afterLoad: function (current) {
            }
        });
    });
    /***DO NOT DELETE*/
     /*$(document).ready(function () {
        var isshow = localStorage.getItem('isshow');
        isshow = null;
        if (isshow == null) {
            localStorage.setItem('isshow', 1);
            $('.fancy').fancybox({
                autoSize: true,
                fitToView: false,
                maxWidth: '100%',
                wrapCSS: 'fancybox-holiday-popup',
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 0, 0.2)'
                        }
                    }
                }
            });
            $('#holidayPopupTrigger').click();
        }
    });*/
    
     $(document).on("click", "#interested-form input[type='submit']", function (e) {
        e.preventDefault();
        var obj = $(this).closest("form");
        var formWrapperObj = obj.closest('.form-container');
        //formWrapperObj.find('.error, .success').html('').hide();
        var formMessageContainerObj = formWrapperObj.find('.form-message-container');
        formMessageContainerObj.html('');

        //$.blockUI({baseZ: 2000});

        var formId = obj.attr('id');
        switch (formId) {
            case 'team-order-form':
                ga('send', 'event', 'Forms', 'submit', 'Team Pre-order Form');
                break
            case 'subscribe-ces-form':
                ga('send', 'event', 'Forms', 'submit', 'Pre-order Form');
                break
            case 'subscribe-form':
                ga('send', 'event', 'Forms', 'submit', 'Subscribe Form');
                break
        }

        obj.ajaxSubmit({
            url: obj.attr('action'),
            dataType: 'json',
            success: function (data) {
               // $.unblockUI();
                if (data.code === 200) {
                    obj[0].reset();
                    formMessageContainerObj.append('<span class="success">' + data.message + '</span>').fadeOut(9000);
                    //obj.find('.successMessage').html(data.message).show().fadeOut(9000);
                } else {
                    $.each(data.errors, function (id, v) {
                        formMessageContainerObj.append('<span class="error">*' + v + '</span>');
                        return false;
                        //obj.find('[id*="_' + id + '_em_"]').html(v).show();
                    });
                }
            }
        });
        return false;
    });

    $(document).ready(function () {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $("iframe").each(function () {
                $(this)[0].contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
            });
        });
    });

});
