function lg(e) {
    return console.log(e);
}
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

Vue.prototype.$http = axios;

Vue.use(VeeValidate);
Vue.directive('popup-click', function (el, binding) {
    var elObj = $(el);
    elObj.on('click', function (event) {
        event.preventDefault();
        $.fancybox.close();
    });
});
Vue.directive('upload-image', function (el, binding) {
    var elObj = $(el);
    var targetObj = elObj;
    var inputObj = elObj.find("input[type='file']");
    var labelObj = elObj.find("label");
    labelObj.attr('for', inputObj.attr('name'));
    inputObj.attr('id', labelObj.attr('for'));
    inputObj.change(function () {
    });
});

var formuploadimage = Vue.extend({
    template: '#template-form-upload-image',
    props: {list: {type: Array}, item: {}, elName: {type: String}, numRequired: {type: Number, default: 1}},
    inject: {
        $validator: '$validator'
    },
    data: function () {
        return {
            validCnt: 0,
            isFileChanged: false,
            // currentList: this.list,
            limitUpload: 11 // 12 inputs per group
        }
    },
    watch: {
        validCnt: function () {
            var vm = this;
            if (vm.validCnt >= vm.numRequired) {
                if (vm.currentList.length === vm.validCnt && vm.currentList.length <= vm.limitUpload) {
                    var id = Math.max.apply(Math, vm.currentList.map(function (c) {
                        return c.id;
                    })) + 1;

                    var newItem = {id: id, required: false, path: '', attachmentId: null, name: ''};
                    vm.currentList.push(newItem);
                    vm.isFileChanged = false;
                }
            }
        },
    },
    computed: {
        isGroupValid: function () {
            var vm = this;
            var isValid = false;
            var isDirty = false;
            var isAllValid = true;
            var list = vm.currentList;
            var validCurrentCnt = 0;
            list.forEach(function (itemObject, index) {
                validCurrentCnt = 0;
                if (itemObject.attachmentId) {
                    isValid = true;
                    isDirty = true;
                } else {
                    isValid =
                            vm.fields.$step3
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']']
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']'].valid;
                    isDirty =
                            vm.fields.$step3
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']']
                            && vm.fields.$step3['installForm[' + vm.elName + '][' + itemObject.id + ']'].dirty;
                }
                itemObject.isValid = isValid;
                itemObject.isDirty = isDirty;
                if (!isValid) {
                    isAllValid = false;
                }
            });
            list.forEach(function (itemObject, index) {
                if (itemObject.isValid == true) {
                    validCurrentCnt = validCurrentCnt + 1;
                }
                if (validCurrentCnt < vm.numRequired) {
                    isAllValid = false;
                } else {
                    isAllValid = true;
                }
            });
            if (vm.isFileChanged === true) {
                vm.validCnt = validCurrentCnt; //to trigger add new input
            }

            return isAllValid;
        },
        currentList: function () {
            var vm = this;
            var list = vm.list;
            var cnt = 0;
            list.forEach(function (itemObject, index) {
                if (itemObject.attachmentId || itemObject.isValid == true) {
                    cnt++
                }
            });
            vm.validCnt = cnt;
            list.forEach(function (itemObject, index) {
                if (index < vm.numRequired) {
                    itemObject.required = true;
                }
            });
            return list;
        }
    },
    methods: {
        onFileChange: function (item, event, name) {
            var vm = this;
            item.isLoading = true;
            vm.isFileChanged = true;
            var dataFile = event.target.files[0];
            setTimeout(function () { //need delay to wait result from computed property
                if (item.isValid == true) {
                    event.target.files = null;
                    var data = new FormData();
                    data.append(name, dataFile);
                    var url = '/installform/saveformMedia?formId=' + vm.$parent.form.id;
                    vm.$http.post(url, data).then(function (response) {
                        var jsonData = response.data;
                        if (jsonData) {
                            item.path = jsonData.path;
                            item.attachmentId = jsonData.id;
                            item.name = jsonData.name;
                            item.isLoading = false;
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }, 50);
        },
        remove: function (item, event) {
            var vm = this;
            item.path = '';

            vm.isGroupValid;

            var url = '/installform/removeformMedia?formId=' + vm.$parent.form.id + '&attachmentId=' + item.attachmentId;
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData.deleted == true) {
                    if (vm.currentList.length > vm.numRequired) {
                        var num = vm.currentList.indexOf(item);
                        vm.currentList.splice(num, 1);
                        // vm.validCnt = vm.validCnt - 1;
                    } else {
                        item.isDirty = false;
                        item.isValid = false;
                        item.attachmentId = null;
                        item.isValid = false;
                    }
                } else {
                    alert('cant delete file')
                }

            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});
var app = new Vue({
    el: ".installform",
    data: {
        shortInfo: {firstName: null, lastName: null, phone: null, email: null, salesNameRep: null},
        haveRequiredPhotosAndVideos: false,
        
        
        facilityName: '',
        contactList: [],
        facilityAddressList: [],
        //facilityAddress: {},
        primaryContactList: [],
        cornerImageList: [],
        cornerImage: {},
        videoView: {path: '', attachmentId: null, name: ''},
        serverRoomImage: {},
        cornerList: [{id: 0, path: '', attachmentId: null, name: ''}, {id: 1, path: '', attachmentId: null, name: ''}, {id: 2, path: '', attachmentId: null, name: ''}, {id: 3, path: '', attachmentId: null, name: ''}],
        hoopsList: [{id: 0, path: '', attachmentId: null, name: ''}, {id: 1, path: '', attachmentId: null, name: ''}],
        serverRoomImgList: [{id: 0, path: '', attachmentId: null, name: ''}],
        currentStep: 1,
        stepsList: {
            step1: {is_valid: false},
            step2: {is_valid: false},
            step3: {is_valid: false}
        },
        isFomValid: false,
        form: {id: null, updated_at: null},
        forms: [],
        timer: {}
    },
    components: {
        formuploadimage: formuploadimage,
    },
    computed: {
        formValid: function () {
            var vm = this;
            var isValid = true;
            var isValidChild = true;
            var fields = vm.fields;
            var list = vm.stepsList;

            Object.keys(list).map(function (scope, index) {
                var fieldScope = vm.fields['$' + scope];
                if (fieldScope) {
                    Object.keys(fieldScope).forEach(function (key) {
                        if (fieldScope[key]) {
                            //console.log(fieldScope[key]);
                            if (fieldScope[key].valid === false || (fieldScope[key].valid === null && fieldScope[key].required === true)) {
                                //if (fieldScope[key].valid === false) {
                                isValid = false;
                            }
                        }
                    });
                }
            });

            vm.$children.map(function (child) {
                Object.keys(list).map(function (scope, index) {
                    var fieldScope = child.fields['$' + scope];
                    if (fieldScope) {
                        Object.keys(fieldScope).forEach(function (key) {
                            if (fieldScope[key]) {
                                //console.log(fieldScope[key]);
                                if (fieldScope[key].valid === false || (fieldScope[key].valid === null && fieldScope[key].required === true)) {
                                    // if (fieldScope[key].valid === false) {
                                    isValidChild = false;
                                }
                            }
                        });
                    }
                    ;
                });
            });

            if (isValid === false || isValidChild === false)
                return false;
            else
                return true;
        },
        isShortInfoValid: function () {
            var vm = this;
            var isValid = null;
            isValid = vm.fields.$step3
                    && vm.fields.$step3['installForm[firstName]']
                    && vm.fields.$step3['installForm[firstName]'].valid
                    && vm.fields.$step3['installForm[lastName]'].valid
                    && vm.fields.$step3['installForm[phone]'].valid
                    && vm.fields.$step3['installForm[email]'].valid
                    && vm.fields.$step3['installForm[salesNameRep]'].valid;
            return isValid;
        }
    },
    watch: {
        cornerList: function () {
            console.log('cornerList updated');
        },
        'form.id': function () {
            this.fetchForm();
        },
        form: function () {
            //this.populateForm();
        },
    },
    created: function () {
        this.getFormId();
        //this.fetchForms();
        // this.timer = setInterval(this.autosaveForm, 20000);
    },
    methods: {
        buildApiUrl: function (url) {
            return url;
        },
        getFormId: function () {
            var vm = this;
            vm.form.id = $('#install-form-facilityimages').data('id').toString();
        },
        initForm: function () {
        },
       /* populateForm: function ()
        {
            var vm = this;

            if (vm.form.updated_at) {
                var primaryContactList = Array.from(vm.form.primary_contact_list);
                if (primaryContactList.length > 0) {
                    vm.primaryContactList = primaryContactList;
                }

                vm.facilityName = vm.form.facility_name;
                vm.facilityAddressList = Array.from(vm.form.facility_address);
                vm.coachingContactList = Array.from(vm.form.coaching_contact_list);
                vm.ITContactList = Array.from(vm.form.it_contact_list);
                vm.athleticDirectorContactList = Array.from(vm.form.athletic_director_contact_list);
                vm.sportsDirectorContactList = Array.from(vm.form.sports_director_contact_list);
                vm.facilityManagerContactList = Array.from(vm.form.facility_manager_contact_list);

                var formInternetSpeed = JSON.parse(vm.form.internet_speed);
                if (formInternetSpeed.upload)
                    vm.internetSpeed.upload = formInternetSpeed.upload;
                if (formInternetSpeed.download)
                    vm.internetSpeed.download = formInternetSpeed.download;
                if (vm.form.equipment_requirements == 'needObtain')
                    vm.needObtain = 1;
                if (vm.form.equipment_requirements == 'locatedOnSite')
                    vm.locatedOnSite = 1;
                if (vm.form.gym_blueprint == 1)
                    vm.isBlueprintRequired = true;

                if (vm.form.media.VIDEO && vm.form.media.VIDEO.length > 0) {
                    vm.videoView.attachmentId = vm.form.media.VIDEO[0].id;
                    vm.videoView.name = vm.form.media.VIDEO[0].title;
                    vm.videoView.path = vm.form.media.VIDEO[0].data;
                    vm.$validator.flag('step3.installForm[video][0]', {
                        valid: true,
                        dirty: true
                    });
                }

                for (var property in vm.form.media) {
                    if (vm.form.media.hasOwnProperty(property)) {
                        var list = null;
                        if (property === 'HOOPS') {
                            var list = vm.hoopsList;
                        }
                        if (property === 'CORNER') {
                            var list = vm.cornerList;
                        }
                        if (property === 'SERVERROOMIMG') {
                            var list = vm.serverRoomImgList;
                        }
                        if (property === 'BLUEPRINT') {
                            vm.blueprintList = vm.form.media[property];
                            if (vm.blueprintList.length > 0) {
                                vm.$validator.flag('step2.installForm[blueprint][]', {
                                    valid: true,
                                    dirty: true
                                });
                            }
                        }

                        var i = 0;
                        if (vm.form.media[property] && list) {
                            vm.form.media[property].forEach(function (item) {
                                var newItem = {id: i, required: false, path: item.data, attachmentId: item.id, name: item.title, isValid: true, isDirty: true};
                                if (list[i]) {
                                    var tmp = list[i];
                                    tmp.id = i;
                                    tmp.required = newItem.required;
                                    tmp.path = item.data;
                                    tmp.attachmentId = item.id;
                                    tmp.name = item.title;
                                    tmp.isValid = item.isValid;
                                    tmp.isDirty = item.isDirty;
                                    vm.$set(list, i, tmp);
                                } else {
                                    list.push(newItem);
                                }
                                i++;
                            });
                        }
                    }
                }
                setTimeout(function () {
                    vm.validateForm('step1', false);
                }, 300);
            }
        },
     */
        onFileChange: function (item, event, name) {
            var vm = this;
            var stepname = 'step3.' + name; //'step3.installForm[video][0]'
            var dataFile = event.target.files[0];
            event.target.files = null;
            item.isLoading = true;
            setTimeout(function () { //need delay to wait result from computed property
                document.getElementById(name).value = "";
                item.isValid = vm.fields.$step3[name].valid;
                vm.$validator.flag(stepname, {//to prevent show valid flag while video uploading
                    valid: false,
                    dirty: true
                });
                if (item.isValid == true) {
                    var data = new FormData();
                    data.append(name, dataFile);
                    var url = '/installform/saveformMedia?formId=' + vm.form.id;
                    vm.$http.post(url, data).then(function (response) {
                        var jsonData = response.data;
                        if (jsonData) {
                            item.path = jsonData.path;
                            item.attachmentId = jsonData.id;
                            item.name = jsonData.name;
                            vm.$validator.flag(stepname, {
                                valid: true,
                                dirty: true
                            });
                            item.isLoading = false;
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }, 100);
        },
        removeVideo: function (item, name) {
            var vm = this;
            var url = '/installform/removeformMedia?formId=' + vm.form.id + '&attachmentId=' + item.attachmentId;
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData.deleted == true) {
                    item.path = '';
                    item.isDirty = null;
                    item.isValid = null;
                    item.attachmentId = null;
                    vm.$validator.flag(name, {
                        valid: false,
                        dirty: false
                    });
                } else {
                    alert('cant delete this file');
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
       /* autosaveForm: function () {
            if (this.form.id > 0) {
                this.saveForm();
            }
        },*/
        fetchForm: function () {
            var vm = this;
            var url = vm.buildApiUrl('/installform/preloadFacilityimagesForm?formId=' + vm.form.id);
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData)
                    vm.form = jsonData;
            }).catch(function (error) {
                console.log(error);
            });
        },

        submitForm: function () {
            var vm = this;
            var list = vm.stepsList;
            Object.keys(list).map(function (objectKey, index) {
                vm.validateForm(objectKey);
            });
            if (vm.formValid) {
                window.onbeforeunload = null;
                $.blockUI({baseZ: 2000});
                document.getElementById("install-form-facilityimages").submit();
            }
        },
        validateForm: function (scope, jumpToNextStep) {
            var vm = this;
            vm.stepsList[scope].is_valid = false;
            this.$validator.validateAll(scope).then(function (result) {
                if (result) {
                    vm.stepsList[scope].is_valid = true;
                    if (jumpToNextStep !== false) {
                        vm.toNextStep();
                    }
                }
            });
        },
               toNextStep: function () {
            if (this.currentStep < 3) {
                this.currentStep += 1;
                document.body.scrollTop = document.documentElement.scrollTop = 0;

            }
        },
        /*createForm: function () {
            var vm = this;
            /* var url = '/installform/create?formId=' + vm.form.id;
             window.onbeforeunload = null;
             window.location.replace(url);*/
          /*  var url = vm.buildApiUrl('/installform/create');
            this.$http.get(url).then(function (response) {
                var jsonData = response.data;
                if (jsonData)
                    vm.form = jsonData;
            }).catch(function (error) {
                console.log(error);
            });
        },*/
        saveForm: function () {
            var vm = this;
            console.log('save-form');
            var options = {
                success: showResponse, // post-submit callback 
                url: '/installform/savefacilityimagesform?formId=' + vm.form.id, // override for form's 'action' attribute 
                clearForm: false, // clear all form fields after successful submit 
                resetForm: false       // reset the form after successful submit 
            };
            var res = '';
            function showResponse(responseText, statusText, xhr, $form) {
                res = JSON.parse(responseText);
                if (res.status == 200) {
                    if (res.form.updated_at) {
                        vm.form.updated_at = res.form.updated_at;
                    }
                    //vm.updateFormDetails(res.form);
                }
            }
            $('#install-form-facilityimages').ajaxSubmit(options);
        }
    }
});