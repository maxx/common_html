function ready() {

    $(function(){
        $("#content-tabs-acc").tabs({
            collapsible: true
        });
    });

    $('.acc-table-title-controls li').on('click', function(e){
       if (e.target.className != 'ui-tabs-anchor') {
           if ($(this).hasClass('ui-state-active')) {

           } else {
               $(this).children().click();
           }
       }
    });

    $(".check-not, .tpp-not p").on('click', function(){
        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
        } else {
            $(this).parent().addClass('active');
        }
    });

    $('.dev16 .sres-head').on('click', function(){
        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
        } else {
            $(this).parent().addClass('active');
        }
    });

    $('.dev16 .rold-dr-but').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.dev16 .role-dropdown-content').css('display','none');
        } else {
            $(this).addClass('active');
            $('.dev16 .role-dropdown-content').css('display','block');
        }
    });

    $('.dev16 .role-dropdown-content a').on('click', function() {
        if ($('.rold-dr-but').hasClass('active')) {
            $('.rold-dr-but').removeClass('active');
        } else {
            $('.rold-dr-but').addClass('active');
        }
        $('.dev16 .role-dropdown-content').css('display','none');
        $('.rold-dr-but').html($(this).html());
    });

}

document.addEventListener("DOMContentLoaded", ready);