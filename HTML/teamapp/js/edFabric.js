function readyFabric() {
    /*
    Init CHART canvas
     */
    var canvas = new fabric.StaticCanvas('field');
    var canvasMap = new fabric.Canvas('fieldMap');
    var zoneURL =  ['img/svg/1.svg',
        'img/svg/2.svg',
        'img/svg/3.svg',
        'img/svg/4.svg',
        'img/svg/5.svg',
        'img/svg/6.svg',
        'img/svg/7.svg',
        'img/svg/8.svg',
        'img/svg/9.svg',
        'img/svg/10.svg',
        'img/svg/11.svg',
        'img/svg/12.svg',
        'img/svg/13.svg',
        'img/svg/14.svg',
        'img/svg/0.svg'
    ];
    var zonemapURL = 'img/svg/00.svg';
    //1170
    var zoneCoordsX = [
        '598',//1
        '353',//2
        '598',//3
        '853',//4
        '216',//5
        '306',//6
        '598',//7
        '893',//8
        '983',//9
        '98',//10
        '183',///11
        '598',//12
        '1003',//13
        '1099'//14
    ]
    //840
    var zoneCoordsY = [
        '160',//1
        '190',//2
        '360',//3
        '190',//4
        '130',//5
        '360',//6
        '480',//7
        '360',//8
        '130',//9
        '100',//10
        '520',///11
        '610',//12
        '520',//13
        '100'//14
    ];

    /*
    INIT colors
     */
    var numberOfItems = 100;
    var rainbow = new Rainbow();
    rainbow.setNumberRange(1, numberOfItems);
    rainbow.setSpectrum('#41e4de', '#eaec38', '#f32a25');

    /*
    Object selector
     */
    fabric.Canvas.prototype.getItemByName = function(name) {
        var object = null,
            objects = this.getObjects();

        for (var i = 0, len = this.size(); i < len; i++) {
            if (objects[i].name && objects[i].name === name) {
                object = objects[i];
                break;
            }
        }

        return object;
    };

    /*
    Function to render CHART
     */
    function renderField(zonePercents, canvas, fs) {
        i = 0;
        window.total = 0;
        while (i < (zoneURL.length-1)) {
                fabric.loadSVGFromURL(zoneURL[i], function(objects, options) {
                    var shape = fabric.util.groupSVGElements(objects, options);
                    shape.scaleToWidth(canvas.width);
                    var hexColour = rainbow.colourAt(zonePercents[options.svgUid]);
                    var fillColor = '#'+hexColour;
                    for (var i = 0; i < shape.paths.length; i++) {
                        shape.paths[i].setFill(fillColor);
                    }
                    shape.set({
                       selection: false,
                       draggable: false,
                       hasControls: false,
                       hasBorders: false
                    });
                    canvas.add(shape);
                    window.total++;
                });

            i++;
        }
        fabric.loadSVGFromURL(zoneURL[i], function(objects, options) {
                var shape = fabric.util.groupSVGElements(objects, options);
                shape.scaleToWidth(canvas.width);

            shape.set({
                selection: false
            });
            canvas.add(shape);
        });

        var heightCoef = ((canvas.width-30)/1165)*60;
        new fabric.Image.fromURL("img/charttop.png", function(img){
            img.left = 23;
            img.top = 0;
            img.width = (canvas.width-30);
            img.height = heightCoef;
            canvas.add(img);
        });
        canvas.renderAll();
        tC = setInterval(function(){
            if (window.total > 13) {
                clearInterval(tC);
                i = 0;
                while (i < (zoneURL.length - 1)) {
                    x = zoneCoordsX[i] * (canvas.width/1170);
                    y = zoneCoordsY[i] * (canvas.height/840);
                    txt = zonePercents[i]+'%';
                    tmp = new fabric.Text(txt, {
                        fontFamily: 'QswaldLight',
                        left: x,
                        top: y,
                        fill: '#fff',
                        originX: 'center',
                        fontSize: fs
                    });
                    canvas.add(tmp);
                    i++;
                }
                canvas.renderAll();
            }
        }, 250);
    }

    /*
    Call example
     */
    var zonePercents = [5,
        13,
        21,
        29,
        37,
        44,
        52,
        60,
        68,
        76,
        83,
        90,
        91,
        2
    ];
    renderField(zonePercents, canvas, 36);

    /*
     Function to render ZONE MAP
     */
    function renderMap(zoneText, zonePercents, canvasMap, fs) {


        fabric.loadSVGFromURL(zonemapURL, function(objects, options) {
            var shape = fabric.util.groupSVGElements(objects, options);
            shape.scaleToWidth(canvasMap.width);
            shape.set({
                fill: '#222',
                selectable: false,
                hoverCursor: 'default',
                draggable: false,
                hasControls: false
            });
            shape.set({
                selectable: false
            });
            canvasMap.add(shape);
            i = 0;
            window.total = 0;
            while (i < (zoneURL.length-1)) {
                var hexColour = rainbow.colourAt(zonePercents[i]);
                var fillColor = '#' + hexColour;
                x = zoneCoordsX[i] * (canvasMap.width/1170);
                y = zoneCoordsY[i] * (canvasMap.height/840) + 20;
                var circle = new fabric.Circle({
                    radius: 20,
                    fill: fillColor,
                    originX: 'center',
                    originY: 'center',
                    left: x,
                    top: y,
                    dataSelector: i,
                    selectable: false,
                    draggable:false,
                    hasControls: false,
                    hoverCursor: 'pointer'
                });

                canvasMap.add(circle);
                window.total++;
                i++;
            }
        });



        canvasMap.renderAll();

        window.mhf = true;
        canvasMap.on('mouse:over', function(e) {
            setTimeout(function () {
                if (e.target !== null) {
                    if (e.target.dataSelector !== undefined) {
                        i = e.target.dataSelector;
                        if (window.mhf) {
                            window.mhf = false;

                            x = zoneCoordsX[i] * (canvasMap.width/1170);
                            y = zoneCoordsY[i] * (canvasMap.height/840) + 20;

                            txt = zoneTextEx[i];

                            canvasMap.remove(canvasMap.getItemByName('rectPAD'));
                            canvasMap.remove(canvasMap.getItemByName('textPAD'));
                            canvasMap.remove(canvasMap.getItemByName('rect2PAD'));
                            canvasMap.renderAll();

                            var rectPAD = new fabric.Image.fromURL("img/map-dot-pad.png", function(img){
                                img.left = (x - 65);
                                img.top = (y - 60);
                                img.width = 138;
                                img.height = 106;
                                img.name = 'rectPAD';
                                img.dataSelector = 'hoverpad';
                                img.dataSelector2 = i;
                                canvasMap.add(img);

                                var textPAD = new fabric.Text(txt, {
                                    fontFamily: 'QswaldLight',
                                    left: (x + 5),
                                    top: (y - 50),
                                    fill: '#222',
                                    originX: 'center',
                                    fontSize: fs,
                                    name: 'textPAD',
                                    dataSelector2: i
                                });

                                var rect2PAD = new fabric.Rect({
                                    left: (x - 65),
                                    top: (y - 60),
                                    width: 138,
                                    height: 106,
                                    fill: 'rgba(0,0,0,0)',
                                    dataSelector: 'hoverpad',
                                    selectable: false,
                                    draggable:false,
                                    hasControls: false,
                                    hoverCursor: 'pointer'
                                });
                                canvasMap.add(textPAD);
                                canvasMap.add(rect2PAD);
                                canvasMap.renderAll();
                            });
                        }
                    }
                }
            }, 50);
        });

        canvasMap.on('mouse:out', function(e) {
                if (e.target !== null) {
                    if (i = e.target.dataSelector) {
                        if (i == 'hoverpad') {
                            canvasMap.remove(canvasMap.getItemByName('rectPAD'));
                            canvasMap.remove(canvasMap.getItemByName('textPAD'));
                            canvasMap.remove(canvasMap.getItemByName('rect2PAD'));
                            canvasMap.renderAll();
                            setTimeout(function () {
                                window.mhf = true;
                            }, 10);
                        }
                    }
                }
        });
    }

    /*
    Call example
     */
    var zoneTextEx = [
        '20/40 | 50%',
        '10/40 | 25%',
        '20/40 | 50%',
        '10/40 | 25%',
        '20/40 | 50%',
        '10/40 | 25%',
        '20/40 | 50%',
        '10/40 | 25%',
        '20/40 | 50%',
        '10/40 | 25%',
        '20/40 | 50%',
        '10/40 | 25%',
        '20/40 | 50%',
        '20/40 | 50%'
    ]
    renderMap(zoneTextEx, zonePercents, canvasMap, 28);



}

document.addEventListener("DOMContentLoaded", readyFabric);