function fieldsRender() {
    generateField({
        zone1: 99,
        zone2: 11,
        zone3: 42,
        zone4: 59,
        zone5: 67,
        zone6: 50,
        zone7: 12,
        zone8: 9,
        zone9: 51,
        zone10: 82,
        zone11: 94,
        zone12: 39,
        zone13: 86,
        zone14: 63
    });

    generateMap({
        zone1: 99,
        zone2: 11,
        zone3: 42,
        zone4: 59,
        zone5: 67,
        zone6: 50,
        zone7: 12,
        zone8: 9,
        zone9: 51,
        zone10: 82,
        zone11: 94,
        zone12: 39,
        zone13: 86,
        zone14: 63
    });
}

document.addEventListener("DOMContentLoaded", fieldsRender);
