

/*
SHOTTRACKER CANVAS
 */

function generateField(zones) {

    // Config
    var cfg = {
        text: {
            // Font size
            size: 24,
            // Text margin
            offset: [-5, -5]
        },
        fieldMargin: {
            top: 90
        },
        strokeColor: '#545454'
    };
    
    // Colors
    colors = [
        '#46d8fe',
        '#58d8e5',
        '#6fd8c5',
        '#7bd8b4',

        '#87d8a5',
        '#9cd887',
        '#aed86e',
        
        '#c4d851',
        '#dad833',
        '#eed817',

        '#fed400',
        '#fabb00',
        '#f7a700',

        '#f38f00',
        '#f17d00',
        '#ec6200',

        '#e94b00',
        '#e53200',
        '#e11a00',

        '#dd0200'
    ];

    if (zones === undefined) {
        // Make 14 zones if params is undefined
        zones = {};
        for (var i = 1; i < 15; i++) {
            zones['zone' + i.toString()] = 100;
        }
    }

    // Zone colors
    var zoneColors = {};
    for (var i = 1; i < 15; i++) {
        zoneColors['zone' + i] = colors[Math.round(zones['zone' + i] / 5)];
    }

    var LC = LibCanvas.extract({});

    // DRAWING
    atom.dom(function() {
        var canvas  = atom.dom('#fieldMap').first;
        var context = canvas.getContext('2d-libcanvas');

        // top color rectangles
        var tpColors = ['#46d8fe', '#87d8a5', '#c4d851', '#fed400', '#f38f00', '#e94b00', '#dd0200'];

        // top line
        var topLine = new LC.Rectangle({
            center: new LC.Point(369, 59),
            size  : new LC.Size(740, 15)
        });
        context.fill(topLine, 'rgba(0,0,0,1)').set({ lineWidth: 0 });

        var gradient = context.createLinearGradient(topLine).addColorStop({
            '0.0' : tpColors[0],
			'0.2' : tpColors[1],
			'0.4' : tpColors[2],
			'0.6' : tpColors[3],
            '0.8' : tpColors[4],
            '0.9' : tpColors[5],
			'1.0' : tpColors[6]
        });
        context.fill(topLine, gradient);

        var tp1 = new LC.Rectangle({
            center: new LC.Point(10, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp1, tpColors[0]).set({ lineWidth: 0 });
        var tp2 = new LC.Rectangle({
            center: new LC.Point(130, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp2, tpColors[1]).set({ lineWidth: 0 });
        var tp3 = new LC.Rectangle({
            center: new LC.Point(250, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp3, tpColors[2]).set({ lineWidth: 0 });
        var tp4 = new LC.Rectangle({
            center: new LC.Point(370, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp4, tpColors[3]).set({ lineWidth: 0 });
        var tp5 = new LC.Rectangle({
            center: new LC.Point(490, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp5, tpColors[4]).set({ lineWidth: 0 });
        var tp6 = new LC.Rectangle({
            center: new LC.Point(610, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp6, tpColors[5]).set({ lineWidth: 0 });
        var tp7 = new LC.Rectangle({
            center: new LC.Point(728, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp7, tpColors[6]).set({ lineWidth: 0 });

        // bck
        var bck = new LC.Rectangle({
            center: new LC.Point(370, 255 + cfg.fieldMargin.top),
            size  : new LC.Size(736, 510)
        });
        context.fill(bck, 'rgba(0,0,0,0)').set({ lineWidth: 2 });
        context.stroke(bck, cfg.strokeColor);
        context.clip(bck);

        // zone13
        var zone13 = new LC.Rectangle({
            center: new LC.Point(556, 306 + cfg.fieldMargin.top),
            size  : new LC.Size(374, 416 + cfg.fieldMargin.top)
        });
        context.fill(zone13, zoneColors.zone13).set({ lineWidth: 2 });
        context.stroke(zone13, cfg.strokeColor);   
        // zone13 text
        context.text({
            text: zones['zone13'] + '%',
            color: '#fff',
            padding: [300 + cfg.text.offset[0] + cfg.fieldMargin.top, 640 + cfg.text.offset[1]],
            size: cfg.text.size
        }); 

        // zone11
        var zone11 = new LC.Rectangle({
            center: new LC.Point(182, 306 + cfg.fieldMargin.top),
            size  : new LC.Size(374, 416 + cfg.fieldMargin.top)
        });
        context.fill(zone11, zoneColors.zone11).set({ lineWidth: 2 });
        context.stroke(zone11, cfg.strokeColor);
        // zone11 text
        context.text({
            text: zones['zone11'] + '%',
            color: '#fff',
            padding: [300 + cfg.text.offset[0] + cfg.fieldMargin.top, 70 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone14
        var zone14 = new LC.Rectangle({
            center: new LC.Point(556, 48 + cfg.fieldMargin.top),
            size  : new LC.Size(374, 100 + cfg.fieldMargin.top)
        });
        context.fill(zone14, zoneColors.zone14).set({ lineWidth: 2 });
        context.stroke(zone14, cfg.strokeColor);
        // zone14 text
        context.text({
            text: zones['zone14'] + '%',
            color: '#fff',
            padding: [24 + cfg.text.offset[0] + cfg.fieldMargin.top, 683 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone10
        var zone10 = new LC.Rectangle({
            center: new LC.Point(182, 48 + cfg.fieldMargin.top),
            size  : new LC.Size(374, 100 + cfg.fieldMargin.top)
        });
        context.fill(zone10, zoneColors.zone10).set({ lineWidth: 2 });
        context.stroke(zone10, cfg.strokeColor);
        // zone10 text
        context.text({
            text: zones['zone10'] + '%',
            color: '#fff',
            padding: [24 + cfg.text.offset[0] + cfg.fieldMargin.top, 20 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone12
        var zone12 = new LC.Polygon([
            new LC.Point(151, 512 + cfg.fieldMargin.top),
            new LC.Point(589, 512 + cfg.fieldMargin.top),
            new LC.Point(370, 137 + cfg.fieldMargin.top)
        ]);
        context.fill(zone12, zoneColors.zone12).set({ lineWidth: 2 });
        context.stroke(zone12, cfg.strokeColor);
        // zone12 text
        context.text({
            text: zones['zone12'] + '%',
            color: '#fff',
            padding: [430 + cfg.text.offset[0] + cfg.fieldMargin.top, 355 + cfg.text.offset[1]],
            size: cfg.text.size
        }); 
        
        // stroke for bbcz
        var bbcz = new LC.Circle({
            center: new LC.Point(370, 86 + cfg.fieldMargin.top),
            radius: 290
        });
        context.fill(bbcz, '#5a5a5a').set({ lineWidth: 5 });
        context.stroke(bbcz, cfg.strokeColor);
        context.clip();

        // big-bigCenterZone
        var bbcz = new LC.Circle({
            center: new LC.Point(370, 86 + cfg.fieldMargin.top),
            radius: 290
        });
        context.fill(bbcz, '#5a5a5a').set({ lineWidth: 2 });
        context.stroke(bbcz, cfg.strokeColor);
        context.clip();

        // zone5
        var zone5 = new LC.Polygon([
            new LC.Point(66, -65 + cfg.fieldMargin.top),
            new LC.Point(45, 219 + cfg.fieldMargin.top),
            new LC.Point(369, 118 + cfg.fieldMargin.top)
        ]);
        context.fill(zone5, zoneColors.zone5).set({ lineWidth: 2 });
        context.stroke(zone5, cfg.strokeColor);
        // zone5 text
        context.text({
            text: zones['zone5'] + '%',
            color: '#fff',
            padding: [50 + cfg.text.offset[0] + cfg.fieldMargin.top, 100 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone9
        var zone9 = new LC.Polygon([
            new LC.Point(701, 216 + cfg.fieldMargin.top),
            new LC.Point(680, -65 + cfg.fieldMargin.top),
            new LC.Point(369, 118 + cfg.fieldMargin.top)
        ]);
        context.fill(zone9, zoneColors.zone9).set({ lineWidth: 2 });
        context.stroke(zone9, cfg.strokeColor);
        // zone9 text
        context.text({
            text: zones['zone9'] + '%',
            color: '#fff',
            padding: [50 + cfg.text.offset[0] + cfg.fieldMargin.top, 610 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone8
        var zone8 = new LC.Polygon([
            new LC.Point(679, 209 + cfg.fieldMargin.top),
            new LC.Point(514, 400 + cfg.fieldMargin.top),
            new LC.Point(370, 118 + cfg.fieldMargin.top)
        ]);
        context.fill(zone8, zoneColors.zone8).set({ lineWidth: 2 });
        context.stroke(zone8, cfg.strokeColor);
        // zone8 text
        context.text({
            text: zones['zone8'] + '%',
            color: '#fff',
            padding: [240 + cfg.text.offset[0] + cfg.fieldMargin.top, 545 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone6
        var zone6 = new LC.Polygon([
            new LC.Point(56, 211 + cfg.fieldMargin.top),
            new LC.Point(221, 400 + cfg.fieldMargin.top),
            new LC.Point(373, 118 + cfg.fieldMargin.top)
        ]);
        context.fill(zone6, zoneColors.zone6).set({ lineWidth: 2 });
        context.stroke(zone6, cfg.strokeColor);
        // zone6 text
        context.text({
            text: zones['zone6'] + '%',
            color: '#fff',
            padding: [240 + cfg.text.offset[0] + cfg.fieldMargin.top, 165 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone7
        var zone7 = new LC.Polygon([
            new LC.Point(151, 512 + cfg.fieldMargin.top),
            new LC.Point(589, 512 + cfg.fieldMargin.top),
            new LC.Point(370, 137 + cfg.fieldMargin.top)
        ]);
        context.fill(zone7, zoneColors.zone7).set({ lineWidth: 2 });
        context.stroke(zone7, cfg.strokeColor);
        // zone7 text
        context.text({
            text: zones['zone7'] + '%',
            color: '#fff',
            padding: [325 + cfg.text.offset[0] + cfg.fieldMargin.top, 355 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        var semicircleMark = new LC.Circle({
            center: new LC.Point(370, 292 + cfg.fieldMargin.top),
            radius: 84.5
        });
        context.fill(semicircleMark, 'rgba(0,0,0,0)').set({ lineWidth: 2 });
        context.stroke(semicircleMark, 'rgba(84,84,84,0.5)');

        // stroke for bcz
        var bcz = new LC.Circle({
            center: new LC.Point(370, 83 + cfg.fieldMargin.top),
            radius: 219
        });
        context.fill(bcz, '#5a5a5a').set({ lineWidth: 5 });
        context.stroke(bcz, cfg.strokeColor);

        // bigCenterZone
        var bcz = new LC.Circle({
            center: new LC.Point(370, 83 + cfg.fieldMargin.top),
            radius: 219
        });
        context.fill(bcz, '#5a5a5a').set({ lineWidth: 2 });
        context.stroke(bcz, cfg.strokeColor);
        context.clip();

        // zone2
        var zone2 = new LC.Rectangle({
            center: new LC.Point(220, 130 + cfg.fieldMargin.top),
            size  : new LC.Size(148, 275 + cfg.fieldMargin.top)
        });
        context.fill(zone2, zoneColors.zone2).set({ lineWidth: 2 });
        context.stroke(zone2, cfg.strokeColor);

        // zone4
        var zone4 = new LC.Rectangle({
            center: new LC.Point(520, 130 + cfg.fieldMargin.top),
            size  : new LC.Size(148, 275 + cfg.fieldMargin.top)
        });
        context.fill(zone4, zoneColors.zone4).set({ lineWidth: 2 });
        context.stroke(zone4, cfg.strokeColor);
        
        // zone3
        var zone3 = new LC.Polygon([
            new LC.Point(227, 303 + cfg.fieldMargin.top),
            new LC.Point(513, 303 + cfg.fieldMargin.top),
            new LC.Point(370, 35 + cfg.fieldMargin.top)
        ]);
        context.fill(zone3, zoneColors.zone3).set({ lineWidth: 2 });
        context.stroke(zone3, cfg.strokeColor);
        
        // zone1
        var zone1 = new LC.Circle({
            center: new LC.Point(370, 83 + cfg.fieldMargin.top),
            radius: 128
        });
        context.fill(zone1, zoneColors.zone1).set({ lineWidth: 2 });
        context.stroke(zone1, cfg.strokeColor);

        // Center lines
        var line1 = new LC.Line([285, 0 + cfg.fieldMargin.top], [285, 288 + cfg.fieldMargin.top]);
        context.fill(line1, '#545454').set({ lineWidth: 1 });
        context.stroke(line1, '#545454');
        var line2 = new LC.Line([454, 0 + cfg.fieldMargin.top], [454, 288 + cfg.fieldMargin.top]);
        context.fill(line2, '#545454').set({ lineWidth: 1 });
        context.stroke(line2, '#545454');
        var line3 = new LC.Line([285, 288 + cfg.fieldMargin.top], [454, 288 + cfg.fieldMargin.top]);
        context.fill(line3, '#545454').set({ lineWidth: 1 });
        context.stroke(line3, '#545454');

        // zone1 text
        context.text({
            text: zones['zone1'] + '%',
            color: '#fff',
            padding: [70 + cfg.text.offset[0] + cfg.fieldMargin.top, 355 + cfg.text.offset[1]],
            size: cfg.text.size
        });
        // zone2 text
        context.text({
            text: zones['zone2'] + '%',
            color: '#fff',
            padding: [70 + cfg.text.offset[0] + cfg.fieldMargin.top, 180 + cfg.text.offset[1]],
            size: cfg.text.size
        });
        // zone3 text
        context.text({
            text: zones['zone3'] + '%',
            color: '#fff',
            padding: [230 + cfg.text.offset[0] + cfg.fieldMargin.top, 355 + cfg.text.offset[1]],
            size: cfg.text.size
        });
        // zone4 text
        context.text({
            text: zones['zone4'] + '%',
            color: '#fff',
            padding: [70 + cfg.text.offset[0] + cfg.fieldMargin.top, 525 + cfg.text.offset[1]],
            size: cfg.text.size
        });

    });
}

function generateMap(zones) {

    // Config
    var cfg = {
        text: {
            // Размер всего текста
            size: 24,
            // Смещение всех текстовых боксов
            offset: [-5, -5]
        },
        fieldMargin: {
            top: 90
        },
        strokeColor: '#545454'
    };

    // Colors
    colors = [
        '#46d8fe',
        '#58d8e5',
        '#6fd8c5',
        '#7bd8b4',

        '#87d8a5',
        '#9cd887',
        '#aed86e',

        '#c4d851',
        '#dad833',
        '#eed817',

        '#fed400',
        '#fabb00',
        '#f7a700',

        '#f38f00',
        '#f17d00',
        '#ec6200',

        '#e94b00',
        '#e53200',
        '#e11a00',

        '#dd0200'
    ];

    if (zones === undefined) {
        // Make 14 zones if params is undefined
        zones = {};
        for (var i = 1; i < 15; i++) {
            zones['zone' + i.toString()] = 100;
        }
    }

    // Zone colors
    var zoneColors = {};
    for (var i = 1; i < 15; i++) {
        zoneColors['zone' + i] = colors[Math.round(zones['zone' + i] / 5)];
    }

    var LC = LibCanvas.extract({});

    // DRAWING
    atom.dom(function() {
        var canvas  = atom.dom('#field').first;
        var context = canvas.getContext('2d-libcanvas');

        // top color rectangles
        var tpColors = ['#46d8fe', '#87d8a5', '#c4d851', '#fed400', '#f38f00', '#e94b00', '#dd0200'];

        // top line
        var topLine = new LC.Rectangle({
            center: new LC.Point(369, 59),
            size  : new LC.Size(740, 15)
        });
        context.fill(topLine, 'rgba(0,0,0,1)').set({ lineWidth: 0 });

        var gradient = context.createLinearGradient(topLine).addColorStop({
            '0.0' : tpColors[0],
            '0.2' : tpColors[1],
            '0.4' : tpColors[2],
            '0.6' : tpColors[3],
            '0.8' : tpColors[4],
            '0.9' : tpColors[5],
            '1.0' : tpColors[6]
        });
        context.fill(topLine, gradient);

        var tp1 = new LC.Rectangle({
            center: new LC.Point(10, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp1, tpColors[0]).set({ lineWidth: 0 });
        var tp2 = new LC.Rectangle({
            center: new LC.Point(130, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp2, tpColors[1]).set({ lineWidth: 0 });
        var tp3 = new LC.Rectangle({
            center: new LC.Point(250, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp3, tpColors[2]).set({ lineWidth: 0 });
        var tp4 = new LC.Rectangle({
            center: new LC.Point(370, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp4, tpColors[3]).set({ lineWidth: 0 });
        var tp5 = new LC.Rectangle({
            center: new LC.Point(490, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp5, tpColors[4]).set({ lineWidth: 0 });
        var tp6 = new LC.Rectangle({
            center: new LC.Point(610, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp6, tpColors[5]).set({ lineWidth: 0 });
        var tp7 = new LC.Rectangle({
            center: new LC.Point(728, 25),
            size  : new LC.Size(20, 20)
        });
        context.fill(tp7, tpColors[6]).set({ lineWidth: 0 });

        // bck
        var bck = new LC.Rectangle({
            center: new LC.Point(370, 255 + cfg.fieldMargin.top),
            size  : new LC.Size(736, 510)
        });
        context.fill(bck, 'rgba(0,0,0,0)').set({ lineWidth: 2 });
        context.stroke(bck, cfg.strokeColor);
        context.clip(bck);

        // zone13
        var zone13 = new LC.Rectangle({
            center: new LC.Point(556, 306 + cfg.fieldMargin.top),
            size  : new LC.Size(374, 416 + cfg.fieldMargin.top)
        });
        context.fill(zone13, zoneColors.zone13).set({ lineWidth: 2 });
        context.stroke(zone13, cfg.strokeColor);
        // zone13 text
        context.text({
            text: zones['zone13'] + '%',
            color: '#fff',
            padding: [300 + cfg.text.offset[0] + cfg.fieldMargin.top, 640 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone11
        var zone11 = new LC.Rectangle({
            center: new LC.Point(182, 306 + cfg.fieldMargin.top),
            size  : new LC.Size(374, 416 + cfg.fieldMargin.top)
        });
        context.fill(zone11, zoneColors.zone11).set({ lineWidth: 2 });
        context.stroke(zone11, cfg.strokeColor);
        // zone11 text
        context.text({
            text: zones['zone11'] + '%',
            color: '#fff',
            padding: [300 + cfg.text.offset[0] + cfg.fieldMargin.top, 70 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone14
        var zone14 = new LC.Rectangle({
            center: new LC.Point(556, 48 + cfg.fieldMargin.top),
            size  : new LC.Size(374, 100 + cfg.fieldMargin.top)
        });
        context.fill(zone14, zoneColors.zone14).set({ lineWidth: 2 });
        context.stroke(zone14, cfg.strokeColor);
        // zone14 text
        context.text({
            text: zones['zone14'] + '%',
            color: '#fff',
            padding: [24 + cfg.text.offset[0] + cfg.fieldMargin.top, 683 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone10
        var zone10 = new LC.Rectangle({
            center: new LC.Point(182, 48 + cfg.fieldMargin.top),
            size  : new LC.Size(374, 100 + cfg.fieldMargin.top)
        });
        context.fill(zone10, zoneColors.zone10).set({ lineWidth: 2 });
        context.stroke(zone10, cfg.strokeColor);
        // zone10 text
        context.text({
            text: zones['zone10'] + '%',
            color: '#fff',
            padding: [24 + cfg.text.offset[0] + cfg.fieldMargin.top, 20 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone12
        var zone12 = new LC.Polygon([
            new LC.Point(151, 512 + cfg.fieldMargin.top),
            new LC.Point(589, 512 + cfg.fieldMargin.top),
            new LC.Point(370, 137 + cfg.fieldMargin.top)
        ]);
        context.fill(zone12, zoneColors.zone12).set({ lineWidth: 2 });
        context.stroke(zone12, cfg.strokeColor);
        // zone12 text
        context.text({
            text: zones['zone12'] + '%',
            color: '#fff',
            padding: [430 + cfg.text.offset[0] + cfg.fieldMargin.top, 355 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // stroke for bbcz
        var bbcz = new LC.Circle({
            center: new LC.Point(370, 86 + cfg.fieldMargin.top),
            radius: 290
        });
        context.fill(bbcz, '#5a5a5a').set({ lineWidth: 5 });
        context.stroke(bbcz, cfg.strokeColor);
        context.clip();

        // big-bigCenterZone
        var bbcz = new LC.Circle({
            center: new LC.Point(370, 86 + cfg.fieldMargin.top),
            radius: 290
        });
        context.fill(bbcz, '#5a5a5a').set({ lineWidth: 2 });
        context.stroke(bbcz, cfg.strokeColor);
        context.clip();

        // zone5
        var zone5 = new LC.Polygon([
            new LC.Point(66, -65 + cfg.fieldMargin.top),
            new LC.Point(45, 219 + cfg.fieldMargin.top),
            new LC.Point(369, 118 + cfg.fieldMargin.top)
        ]);
        context.fill(zone5, zoneColors.zone5).set({ lineWidth: 2 });
        context.stroke(zone5, cfg.strokeColor);
        // zone5 text
        context.text({
            text: zones['zone5'] + '%',
            color: '#fff',
            padding: [50 + cfg.text.offset[0] + cfg.fieldMargin.top, 100 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone9
        var zone9 = new LC.Polygon([
            new LC.Point(701, 216 + cfg.fieldMargin.top),
            new LC.Point(680, -65 + cfg.fieldMargin.top),
            new LC.Point(369, 118 + cfg.fieldMargin.top)
        ]);
        context.fill(zone9, zoneColors.zone9).set({ lineWidth: 2 });
        context.stroke(zone9, cfg.strokeColor);
        // zone9 text
        context.text({
            text: zones['zone9'] + '%',
            color: '#fff',
            padding: [50 + cfg.text.offset[0] + cfg.fieldMargin.top, 610 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone8
        var zone8 = new LC.Polygon([
            new LC.Point(679, 209 + cfg.fieldMargin.top),
            new LC.Point(514, 400 + cfg.fieldMargin.top),
            new LC.Point(370, 118 + cfg.fieldMargin.top)
        ]);
        context.fill(zone8, zoneColors.zone8).set({ lineWidth: 2 });
        context.stroke(zone8, cfg.strokeColor);
        // zone8 text
        context.text({
            text: zones['zone8'] + '%',
            color: '#fff',
            padding: [240 + cfg.text.offset[0] + cfg.fieldMargin.top, 545 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone6
        var zone6 = new LC.Polygon([
            new LC.Point(56, 211 + cfg.fieldMargin.top),
            new LC.Point(221, 400 + cfg.fieldMargin.top),
            new LC.Point(373, 118 + cfg.fieldMargin.top)
        ]);
        context.fill(zone6, zoneColors.zone6).set({ lineWidth: 2 });
        context.stroke(zone6, cfg.strokeColor);
        // zone6 text
        context.text({
            text: zones['zone6'] + '%',
            color: '#fff',
            padding: [240 + cfg.text.offset[0] + cfg.fieldMargin.top, 165 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        // zone7
        var zone7 = new LC.Polygon([
            new LC.Point(151, 512 + cfg.fieldMargin.top),
            new LC.Point(589, 512 + cfg.fieldMargin.top),
            new LC.Point(370, 137 + cfg.fieldMargin.top)
        ]);
        context.fill(zone7, zoneColors.zone7).set({ lineWidth: 2 });
        context.stroke(zone7, cfg.strokeColor);
        // zone7 text
        context.text({
            text: zones['zone7'] + '%',
            color: '#fff',
            padding: [325 + cfg.text.offset[0] + cfg.fieldMargin.top, 355 + cfg.text.offset[1]],
            size: cfg.text.size
        });

        var semicircleMark = new LC.Circle({
            center: new LC.Point(370, 292 + cfg.fieldMargin.top),
            radius: 84.5
        });
        context.fill(semicircleMark, 'rgba(0,0,0,0)').set({ lineWidth: 2 });
        context.stroke(semicircleMark, 'rgba(84,84,84,0.5)');

        // stroke for bcz
        var bcz = new LC.Circle({
            center: new LC.Point(370, 83 + cfg.fieldMargin.top),
            radius: 219
        });
        context.fill(bcz, '#5a5a5a').set({ lineWidth: 5 });
        context.stroke(bcz, cfg.strokeColor);

        // bigCenterZone
        var bcz = new LC.Circle({
            center: new LC.Point(370, 83 + cfg.fieldMargin.top),
            radius: 219
        });
        context.fill(bcz, '#5a5a5a').set({ lineWidth: 2 });
        context.stroke(bcz, cfg.strokeColor);
        context.clip();

        // zone2
        var zone2 = new LC.Rectangle({
            center: new LC.Point(220, 130 + cfg.fieldMargin.top),
            size  : new LC.Size(148, 275 + cfg.fieldMargin.top)
        });
        context.fill(zone2, zoneColors.zone2).set({ lineWidth: 2 });
        context.stroke(zone2, cfg.strokeColor);

        // zone4
        var zone4 = new LC.Rectangle({
            center: new LC.Point(520, 130 + cfg.fieldMargin.top),
            size  : new LC.Size(148, 275 + cfg.fieldMargin.top)
        });
        context.fill(zone4, zoneColors.zone4).set({ lineWidth: 2 });
        context.stroke(zone4, cfg.strokeColor);

        // zone3
        var zone3 = new LC.Polygon([
            new LC.Point(227, 303 + cfg.fieldMargin.top),
            new LC.Point(513, 303 + cfg.fieldMargin.top),
            new LC.Point(370, 35 + cfg.fieldMargin.top)
        ]);
        context.fill(zone3, zoneColors.zone3).set({ lineWidth: 2 });
        context.stroke(zone3, cfg.strokeColor);

        // zone1
        var zone1 = new LC.Circle({
            center: new LC.Point(370, 83 + cfg.fieldMargin.top),
            radius: 128
        });
        context.fill(zone1, zoneColors.zone1).set({ lineWidth: 2 });
        context.stroke(zone1, cfg.strokeColor);

        // Center lines
        var line1 = new LC.Line([285, 0 + cfg.fieldMargin.top], [285, 288 + cfg.fieldMargin.top]);
        context.fill(line1, '#545454').set({ lineWidth: 1 });
        context.stroke(line1, '#545454');
        var line2 = new LC.Line([454, 0 + cfg.fieldMargin.top], [454, 288 + cfg.fieldMargin.top]);
        context.fill(line2, '#545454').set({ lineWidth: 1 });
        context.stroke(line2, '#545454');
        var line3 = new LC.Line([285, 288 + cfg.fieldMargin.top], [454, 288 + cfg.fieldMargin.top]);
        context.fill(line3, '#545454').set({ lineWidth: 1 });
        context.stroke(line3, '#545454');

        // zone1 text
        context.text({
            text: zones['zone1'] + '%',
            color: '#fff',
            padding: [70 + cfg.text.offset[0] + cfg.fieldMargin.top, 355 + cfg.text.offset[1]],
            size: cfg.text.size
        });
        // zone2 text
        context.text({
            text: zones['zone2'] + '%',
            color: '#fff',
            padding: [70 + cfg.text.offset[0] + cfg.fieldMargin.top, 180 + cfg.text.offset[1]],
            size: cfg.text.size
        });
        // zone3 text
        context.text({
            text: zones['zone3'] + '%',
            color: '#fff',
            padding: [230 + cfg.text.offset[0] + cfg.fieldMargin.top, 355 + cfg.text.offset[1]],
            size: cfg.text.size
        });
        // zone4 text
        context.text({
            text: zones['zone4'] + '%',
            color: '#fff',
            padding: [70 + cfg.text.offset[0] + cfg.fieldMargin.top, 525 + cfg.text.offset[1]],
            size: cfg.text.size
        });

    });
}