function ready() {

    $(function(){
        $("#content-tabs").tabs({
            collapsible: true
        });
    });

    /*
    DEV 12
     */
    $('.ct-buttons.playersb a').on('click', function(){
        $('.ct-buttons.playersb a').removeClass('active');
        $(this).addClass('active');
        
        if ($(this).attr('data-role') == 'show-stats') {
            $('.cstats-wrapper.playersb').css('display','block');
            $('.ccharts-wrapper.playersb').css('display','none');
        } else {
            $('.cstats-wrapper.playersb').css('display','none');
            $('.ccharts-wrapper.playersb').css('display','block');
        }
    });

    $('.ct-buttons.lineupsb a').on('click', function(){
        $('.ct-buttons.lineupsb a').removeClass('active');
        $(this).addClass('active');

        if ($(this).attr('data-role') == 'show-stats') {
            $('.cstats-wrapper.lineupsb').css('display','block');
            $('.ccharts-wrapper.lineupsb').css('display','none');
        } else {
            $('.cstats-wrapper.lineupsb').css('display','none');
            $('.ccharts-wrapper.lineupsb').css('display','block');
        }
    });

    $('.main-table-left-wrapper .dev12-lineups .team-title.lineups').on('click', function(){
        $('.dev12-players').css('display','block');
        $('.dev12-lineups').css('display','none');
    });

    $('.main-table-left-wrapper .dev12-players .team-title.plrs').on('click', function(){
        $('.dev12-players').css('display','none');
        $('.dev12-lineups').css('display','block');
    });

    /*
    DEV 7
     */
        /*$(".progress-bar").loading();*/

        $(".btn-zone").on('click', function(){
            if (!$(this).hasClass('active')) {
                $(".btn-zone").removeClass('active');
                $(this).addClass('active');
                if ($(this).attr('data-type') == 'btn-chart') {
                    $('.chart-content').css('display','block');
                    $('.map-content').css('display','none');
                } else {
                    $('.map-content').css('display','block');
                    $('.chart-content').css('display','none');
                }
            }
        });

    /*
    DEV 11
     */
    $('.players-table-title-controls a').on('click', function() {
       if ($(this).attr('href')=='#tabs-possessions') {
           $('.top-h-posessions').css('display','block');
           $('.top-h-results').css('display','none');
       } else {
           $('.top-h-posessions').css('display','none');
           $('.top-h-results').css('display','block');
       }
    });

    /*

    STATISTIC TAB

     */
    window.progressFlags = [true, true, true];
    $('#pb1').circleProgress({
        value: 0.42,
        size: 120,
        fill: {
            color: "#d76f2c"
        },
        thickness: "18",
        startAngle: -1.5708
    });
    $('#pb2').circleProgress({
        value: 0.28,
        size: 120,
        fill: {
            color: "#d76f2c"
        },
        thickness: "18",
        startAngle: -1.5708
    });
    $('#pb3').circleProgress({
        value: 0.78,
        size: 120,
        fill: {
            color: "#d76f2c"
        },
        thickness: "18",
        startAngle: -1.5708
    });

    /*

    COMPARE TAB

    */
    $('.pb-compare1').circleProgress({
        value: 0.04,
        size: 74,
        fill: {
            color: "#d76f2c"
        },
        thickness: "11",
        startAngle: -1.5708
    });
    $('.pb-compare2').circleProgress({
        value: 0.43,
        size: 74,
        fill: {
            color: "#d76f2c"
        },
        thickness: "11",
        startAngle: -1.5708
    });
    $('.pb-compare3').circleProgress({
        value: 0.25,
        size: 74,
        fill: {
            color: "#d76f2c"
        },
        thickness: "11",
        startAngle: -1.5708
    });
    $('.pb-compare4').circleProgress({
        value: 0.55,
        size: 74,
        fill: {
            color: "#d76f2c"
        },
        thickness: "11",
        startAngle: -1.5708
    });

    $('a[href="#tabs-statistics"]').on('click', function() {
        if (window.progressFlags[0]) {
            $('.dev7 .progress-bar').circleProgress();
            window.progressFlags[0] = false;
        }
    });

    $('a[data-role="show-stats"]').on('click', function() {
        if ($('.dev12-players').css('display') == 'none') {
            if (window.progressFlags[2]) {
                $('.dev12 .progress-bar').circleProgress();
                window.progressFlags[2] = false;
            }
        } else {
            if (window.progressFlags[1]) {
                $('.dev12 .progress-bar').circleProgress();
                window.progressFlags[1] = false;
            }
        }
    });

    $('.progress-bar').on('circle-animation-progress', function(event, progress, stepValue) {
        $(this).find('div span').html(parseInt(100 * stepValue.toFixed(2)) + '<text>%</text>');
    });


/*
    $.fn.extend({
        horizontalPB: function() {
                maxWidth = $(this).attr('data-value');
                var width = 0;
                var id = setInterval(frame($(this)), 10);
                function frame(elem) {
                    if (width >= maxWidth) {
                        clearInterval(id);
                    } else {
                        console.log(elem);
                        width++;
                        elem.css('width', width+'%');
                    }
                }
        }
    });
    $('.testa.cs-progressbar').horizontalPB();
*/


}

document.addEventListener("DOMContentLoaded", ready);