function ready() {
    $(".check").on('click', function(){
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });
}

document.addEventListener("DOMContentLoaded", ready);