(function($) {
  $.fn.customSlider = function(options) {
    var defaults = {
      itemsCnt: 5,
	  action:'',
    };
    var opts = $.extend({}, defaults, options);
    this.each(function() {
      var frame = $(this);
      var view = frame.find("ol");
      var itemsCnt = 5;
      var ulWidth = 0;
      var liCnt = 0;
      view.find('li').each(function() {
        ulWidth = ulWidth + $(this).width();
        liCnt++;
      });
	  if(opts.action==='reset'){
		view.css('left','');
	  }
      view.width(ulWidth * 2.5);
      var slides = Math.ceil(liCnt / opts.itemsCnt);
      var slideWidth = frame.width()-8;	  
      var move = slideWidth+"px";
      var currentPosition = parseInt(view.css("left"));
      var nextBtn = frame.parent().find('.nextPage');
      var prevBtn = frame.parent().find('.prevPage');
      var inClick = false;
      var currentSlide = 1;
      nextBtn.on('click', (function() {
        if (inClick) return;
        inClick = true;
        var currentPosition = parseInt(view.css("left"));
        currentSlide++;
        if (currentSlide <= slides) view.stop(false, true).animate({left: "-=" + move}, 400, function() {inClick = false;})
        else {inClick = false;currentSlide = slides;}
      }));
      prevBtn.on('click', (function() {
        if (inClick) return;
        inClick = true;
        var currentPosition = parseInt(view.css("left"));
        currentSlide = currentSlide - 1;
        if (currentSlide > 0) view.stop(false, true).animate({left: "+=" + move}, 400, function() {inClick = false})
        else {inClick = false;currentSlide = 1;}
      }));
    });
    return this;
  };
}(jQuery));

(function($) {
  $.fn.customVSlider = function(options) {
    var defaults = {
	  action:'',
    };
    var opts = $.extend({}, defaults, options);
    this.each(function() {
      var frame = $(this);
      var view = frame.parent();
	  var viewHeight = frame.height();
      var slideHeight = view.height();
	  var slides = Math.ceil(viewHeight/slideHeight);
	  var scrolled=0;

      var nextBtn = view.parent().find('.downPage');
      var prevBtn = view.parent().find('.upPage');
	  if(opts.action==='reset'){
		 view.animate({
				scrollTop: scrolled
			}, 10);
	  }
	  
      nextBtn.on('click', (function() {
		  scrolled = scrolled+slideHeight;
		  if(scrolled<=viewHeight){
			 view.animate({
				scrollTop: scrolled
			}, 500);
		  } else {
			    scrolled = 0;
		  }
      }));
	  
      prevBtn.on('click', (function() {
		  if (scrolled>=slideHeight){
		  scrolled = scrolled-slideHeight;
			 view.animate({
				scrollTop: scrolled
			}, 500);
		  } else {
			scrolled = 0;
		  }
      }));
    });
    return this;
  };
}(jQuery));