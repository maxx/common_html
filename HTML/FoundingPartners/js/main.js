$(function() {
    "use strict";

    var i = new WOW({
        offset: 120,
        boxClass: "wow",
        animateClass: "animated",
        offset: 100,
        live: !0
    });
    i.init();
	
	$(document).ready(function () {
		$('.fancybox-media').fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                media: {}
            }
        });
    });	
});


