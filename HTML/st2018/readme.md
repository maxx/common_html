ST Website 2018 Update 

Dropbox Link:
https://www.dropbox.com/sh/gq4b757pqofq4qu/AAAThKxoW8AWjdVqphjd_VX8a?dl=0


Links for the FEATURES on the Home Page
1) Advanced Statistical data & insights (link to Coaches page, section with the list of stats)
2) Performance data correlated with statistics (link to same place on coaches page)
3) Automated real-time player reports (to player page)
4) Video Integration (link to coach page)
5) Team, Player and Fan apps
6) Augmented reality (link to fan page)