$(function () {
    'use strict';
    $(document).on("click", "#interested-form input[type='submit']", function (e) {
        e.preventDefault();
        var obj = $(this).closest("form");
        var formWrapperObj = obj.closest('.form-container');
        //formWrapperObj.find('.error, .success').html('').hide();
        var formMessageContainerObj = formWrapperObj.find('.form-message-container');
        formMessageContainerObj.html('');

        //$.blockUI({baseZ: 2000});

        var formId = obj.attr('id');
        switch (formId) {
            case 'team-order-form':
                ga('send', 'event', 'Forms', 'submit', 'Team Pre-order Form');
                break
            case 'subscribe-ces-form':
                ga('send', 'event', 'Forms', 'submit', 'Pre-order Form');
                break
            case 'subscribe-form':
                ga('send', 'event', 'Forms', 'submit', 'Subscribe Form');
                break
        }

        obj.ajaxSubmit({
            url: obj.attr('action'),
            dataType: 'json',
            success: function (data) {
               // $.unblockUI();
               formMessageContainerObj.html('');
                if (data.code === 200) {
                    obj[0].reset();
                    formMessageContainerObj.append('<span class="success">' + data.message + '</span>');
                    //obj.find('.successMessage').html(data.message).show().fadeOut(9000);
                } else {
                    $.each(data.errors, function (id, v) {
                        formMessageContainerObj.append('<span class="error">*' + v + '</span>');
                        return false;
                        //obj.find('[id*="_' + id + '_em_"]').html(v).show();
                    });
                }
            }
        });
        return false;
    });
    $('#subscribe-form, #subscribe-ces-form').on('keyup keypress', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });
});