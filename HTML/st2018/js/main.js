Utilities = {}

Utilities.Common = {
    initcarousel: function (params)
    {
        params = $.extend({
            el: '.carousel',
            items: 3,
            center: false
        }, params);

        var elObj = $(params.el);
        if (elObj.length) {
            var owl = elObj;
            owl.owlCarousel({
                nav: false,
                dots: false,
                items: params.items,
                center: params.center,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 3,
                    },
                    600: {
                        items: 5,
                    },
                    800: {
                        items: 7,
                    },
                },
                loop: true,
                //margin: 10,
                autoplay: true,
                // autoplayTimeout: 1000,
                navSpeed: 1000,
                autoplayHoverPause: false
            });
        }
    },
    socSharePopup: function (params)
    {
        params = $.extend({
        }, params);

        $(document).on("click", params.el, function (event) {
            event.preventDefault();
            window.open($(this).attr("href"), "Sharing", "directories=0,height=600,width=600,location=0,menubar=0,status=0,titlebar=0,toolbar=0");
        });
    },
    fbShareUrl: function (params)
    {
        params = $.extend({
        }, params);
    },
    twitterShareUrl: function (params)
    {
        params = $.extend({
            url: '',
            hashtags: 'ShotTracker,video',
            text: '',
            via: 'twitterdev',
            related: 'twitterapi,twitter'
        }, params);
        var url = 'https://twitter.com/share?url=' + params.url + '&via=' + params.via + '&related=' + params.related + '&hashtags=' + params.hashtags + '&text=' + params.text;
        //'https://twitter.com/share?url=https://vimeo.com/210292887&via=twitterdev&related=twitterapi,twitter&hashtags=example,demo&text=ShotTracker – Endorsed by the NABC';
        return url;
    },
    emailShareUrl: function (params)
    {
        params = $.extend({
            url: '',
            subject:'I wanted you to see this video from ShotTracker',
            text: ''
        }, params);
        
        var url = 'mailto:?subject='+params.subject+'&amp; body=Check out this video '+params.url;
        return url;
    },
    saveTimeZone: function ()
    {
        var tz = jstz.determine();
        var timezone = tz.name();
        $.post(yii.urls.base + "/saveBrowserTimeZone", {tz: timezone}, function (data) {
        });

    },
    millionData: function ()
    {
        $.getJSON(yii.urls.base + "/site/million", function (data) {
            if (data.code === 200) {
                $('.million__intro-counter-inner').html(data.htmlAttemptCount);
                $('.million__intro h1').html(data.h1);
                if (data.finished === 1) {
                    $('.million__intro h1').html(data.h1);
                    $('.million__intro-logo').addClass('t0');
                    //$('#winner').html(data.winner.data.userName);
                    $('#topShooter').parent().hide();
                    $('#lastShooter').parent().hide();
                } else {
                    $('.million__intro h1').html(data.h1);
                    $('.million__intro-logo').removeClass('t0');
                    $('#topShooter').parent().show();
                    $('#lastShooter').parent().show();
                    if (data.topShooter)
                        $('#topShooter').html(data.topShooter.userName + '<u>|</u>' + data.topShooter.attemptCount + ' SHOTS');
                    else
                        $('#topShooter').parent().hide();
                    if (data.lastShooter)
                        $('#lastShooter').html(data.lastShooter.userName + '<u>|</u>' + data.lastShooter.attemptCount + ' SHOTS');
                    else
                        $('#lastShooter').parent().hide();
                }
            }
        });

    },
    millionDataTimer: function ()
    {
        var interval = 1000 * 60 * 1; // where X is your every X minutes
        setInterval(function () {
            Utilities.Common.millionData();
        }, interval);

    },
    showPopup: function (params)
    {
        params = $.extend({
            countryName: ''
        }, params);
        setTimeout(function () {
            $('#def-popup').fadeIn(200);
            $('#def-popup .popup-inner').append('Your country is ' + params.countryName)

            $('.popup-closer').on('click', function () {
                $(this).parent().fadeOut(200);
            });
            $('.popup-inner').css('margin-top', -($('.popup-inner').height() / 2));
            $(document).keyup(function (e) {
                if (e.keyCode == 27) {
                    $('.popup').fadeOut(200);
                }
            });

        }, 20000);
    },
    phoneMask: function (params)
    {
        params = $.extend({
        }, params);
        var pattern = '(999) 999-99999';
        var pattern2 = 'Z999999999999999999';
        var options = {onKeyPress: function (cep, e, field, options) {
                if (cep.length > 14) {
                    $(params.el).unmask(pattern);
                    $(params.el).mask(pattern2, {
                        translation: {
                            'Z': {
                                pattern: /[+]/, optional: true
                            }
                        }
                    });
                } else {
                }
            }};
        $(params.el).mask(pattern, options);
    }
};

$(function () {
    'use strict';
    var x = $('.main-menu').outerHeight();
    $('.header .main-menu').scrollFix();

    function isMobile() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return     true;
        }
        return     false;
    }

    if (isMobile()) {
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            //windowHeight = $(window).innerHeight();
            var bannerHeight = $(".player-app-tiles").outerHeight();
            $('.player-app-tiles .tile-wrapper').each(function () {
                if (scroll > $(this).offset().top - bannerHeight) {
                    $('.player-app-tiles .tile-wrapper').each(function () {
                        $(this).find('.tile').removeClass('active');
                    });
                    var fnd = $(this).find('.tile');
                    fnd.addClass('active');
                } else {
                    $(this).find('.tile').removeClass('active');
                }
            });
        });
    }

    $(document).ready(function () {
        $("[data-fancybox]").fancybox({
        });

        $("[data-fancybox-video]").fancybox({
            baseClass: 'custom-transparent-popup'
        });
    });

    $(document).on("click", '.fancybox-media, .non-fancybox-media', function (e) {
        var obj = $(this);
        var href = obj.attr('href');
        if (href.indexOf('vimeo') !== -1 || href.indexOf('youtu.be') !== -1) {
            var attrTitle = obj.attr('title') ? obj.attr('title') : href;
            //  ga('send', 'event', 'video', 'play', attrTitle);
        }
    });
    function turnOnLogos() {
        $('.home-partnership-slider-container .carousel').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 0,
            speed: 6000,
            cssEase: 'linear',
        });
        $('.slick').slick({
            slidesToShow: 7,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 0,
            speed: 6000,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 4,
                    }
                },
            ]
        });
    }
    function appendHiddenFieldById(params) {
        params = $.extend({
            parentId: 'hidden-field',
            name: 'hidden-name',
            value: ''
        }, params);
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", params.name);
        input.setAttribute("value", params.value);
        var obj = document.getElementById(params.parentId);
        if (obj) {
            obj.appendChild(input);
        }
    }

    $(document).ready(function () {
        turnOnLogos();
        // turnOnSlider();

        if ($('#contact-form').length) {
            appendHiddenFieldById({parentId: 'contact-form', 'name': 'antispam'});
        }

        var $owlPlayers = $('.players-app-slider .owl-carousel');
        $owlPlayers.owlCarousel({
            stagePadding: 130,
            nav: false,
            dots: false,
            center: true,
            loop: true,
            items: 3,
        });

        $(document).on('click', '.nav-list li', function () {
            $('.nav-list li.active').removeClass('active');
            $(this).addClass('active');
            var index = $(this).index();
            $owlPlayers.trigger('to.owl.carousel', index);
            return false;
        });

        /*$(document).on('click', '.videos .video-list a', function (e) {
            e.preventDefault();
            var videoId = $(this).attr('href').match(/\d+/g).map(Number)[0];
            var videoTitle = $(this).attr('title');
            var videoUrl = 'https://vimeo.com/'+videoId;
            
            //$('.main-video').html('<iframe src="https://player.vimeo.com/video/'+videoId+'" id="" frameborder="0"  width="840" height="620"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            //update share btns
            $(".videos .social").find("[data-type='twitter']").attr('href', Utilities.Common.twitterShareUrl({url: videoUrl, text: videoTitle}));
            $(".videos .social").find("[data-type='email']").attr('href', Utilities.Common.emailShareUrl({url: videoUrl}));
            $(".videos .social").find("[data-type='facebook']").attr('data-url', videoUrl).attr('data-title', videoTitle).attr('data-description', videoTitle);
            Utilities.Common.socSharePopup({el: '.videos .social a:not([data-type="email"])'});
           document.body.scrollTop = document.documentElement.scrollTop = 0;
        });*/
        $(document).on('click', '.videos .social a[data-type="facebook"], .share-buttons-group a[data-type="facebook"]', function (e) {
            e.preventDefault();
            var obj = $(this);
            FB.ui({
                method: 'share_open_graph',
                action_type: 'og.shares',
                action_properties: JSON.stringify({
                    object: {
                        'og:url': obj.attr('data-url'),
                        'og:title': obj.attr('data-title'),
                        'og:description': obj.attr('data-description')
                    }
                })
            });
        });
        $(document).ready(function () {
            //$('.videos .video-list a.default').trigger('click');
            $('.faq #headingOne a').trigger('click');
        });
        
        
        /***************************************************************/
        var defaultSlickSpeed = 5000;
        var navSlickSpeed = 500;
        var $slickQuotesCoaches = $('.slick-coaches');
        $slickQuotesCoaches.slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            arrows: false,
            autoplaySpeed: 10000,
            speed: defaultSlickSpeed,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        /*vertical:true,
                         verticalSwiping:true,*/
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
            ]
        });
        $slickQuotesCoaches.parent().find('.slider-right-arrow').click(function () {
            slickNextTrigger($slickQuotesCoaches);
        });

        var $slickQuotesCoachesMobile = $('.slick-coaches-mobile');
        $slickQuotesCoachesMobile.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            arrows: false,
            autoplaySpeed: 10000,
            speed: defaultSlickSpeed,
            cssEase: 'linear',
        });
        $slickQuotesCoachesMobile.parent().find('.slider-right-arrow').click(function () {
            slickNextTrigger($slickQuotesCoachesMobile);
        });


        var $slickQuotesPlayers = $('.slick-players');
        $slickQuotesPlayers.slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            arrows: false,
            autoplaySpeed: 10000,
            speed: defaultSlickSpeed,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
        $slickQuotesPlayers.parent().find('.slider-right-arrow').click(function () {
            slickNextTrigger($slickQuotesPlayers);
        });

        var $slickQuotesAboutUs = $('.slick-about-us');
        $slickQuotesAboutUs.slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            autoplaySpeed: 10000,
            speed: 5000,
            cssEase: 'linear'
        });
        $('.slider-right-arrow').click(function () {
            slickNextTrigger($slickQuotesAboutUs);
        });

        var $slickComponentSliderQuotesPlayers = $('.single-slider');
        $slickComponentSliderQuotesPlayers.slick({
            dots: false,
            nav: true,
            nextArrow: '<span class="slider-right-arrow-dark"></span>',
            prevArrow: '<span class="slider-left-arrow-dark"></span>',
            //items: 1,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });

        var $slickLinkCards = $('.slick-link-cards');
        $slickLinkCards.slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            arrows: false,
            autoplaySpeed: 10000,
            speed: defaultSlickSpeed,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ]
        });
        $slickLinkCards.parent().find('.slider-right-arrow').click(function () {
            slickNextTrigger($slickLinkCards);
        });
        $slickLinkCards.parent().find('.slider-left-arrow').click(function () {
            slickPrevTrigger($slickLinkCards);
        });

        function slickNextTrigger(obj) {
            obj.slick("setOption", "speed", navSlickSpeed);
            obj.slick('slickNext');
            obj.slick("setOption", "speed", defaultSlickSpeed);
        }
        function slickPrevTrigger(obj) {
            obj.slick("setOption", "speed", navSlickSpeed);
            obj.slick('slickPrev');
            obj.slick("setOption", "speed", defaultSlickSpeed);
        }
        $(document).on("click", ".items-download a", function (e) {
            e.preventDefault();
            alert(1);
            console.log('clicked');
            var url = $(this).attr('url');
            startDownload(url);
        });
        function startDownload(url) {
            window.location.href = url;
        }
        /*************************************************************/
    });
    
    $(function () {
        $(".collapse").on('show.bs.collapse', function (e) {
            $(e.target).prev().find("i").removeClass("fa-plus").addClass("fa-minus");
        });
        $(".collapse").on('hidden.bs.collapse', function (e) {
            $(e.target).prev().find("i").removeClass("fa-minus").addClass("fa-plus");
        });
    });

});
function lg(variable) {
    console.log(variable);
}
