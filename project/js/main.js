(function($) {
	$('.countdown').countdown('2015/06/21').on('update.countdown', function (event) {
		$(this).html(event.strftime('<span>%D<span>days</span></span><b>:</b>'+'<span>%H<span>hours</span></span><b>:</b>'+'<span>%M<span>mins</span></span><b>:</b>'+'<span>%S<span>secs</span></span>'));
	});

	$('.questions .collapse').on('shown.bs.collapse', function () {
		$('.questions a[aria-expanded="true"]').each(function(){
			$(this).html('&minus;');
		});
	});

	$('.questions .collapse').on('hidden.bs.collapse', function () {
		$('.questions a[aria-expanded="false"]').each(function(){
			$(this).text('+');
		});
	});

	$('.has-error [data-toggle="tooltip"]').tooltip('show');

	if($('.video-frame')) {$(this).fitVids();}

	$('#signUp .switcher').on('click', function () {
		$('#signUp').fadeOut(0);
		$('#signIn').fadeIn(200);
	});

	$('#signIn .switcher').on('click', function () {
		$('#signIn').fadeOut(0);
		$('#signUp').fadeIn(200);
	});

	var sw = function() {
		$('body').append('<div id="mfdehc"><div id="mfdehd"></div></div>');
		var scrollWidth = $('#mfdehc').width() - $('#mfdehd').width();
		$('#mfdehc').remove();
		return scrollWidth;
	}();

	if ($('.board-pannel__table').height() < $('.board-pannel__table > .board-pannel__table-wrapper').height()) {
		$('.board-pannel__item.leader').css('padding-right', sw + 1);
	}

	var offset = $('.header4').height();
	$('body').scrollspy({ target: '.navbar', offset: offset});
	// $.lockfixed('.header4',{offset: 0});

	$('.stat-number').each(function(){
		var str = $(this).text().replace(',','<u>,</u>');
		$(this).html(str);
	});


	smoothScroll.init({
		speed: 500,
		easing: 'easeInOutCubic',
		updateURL: true,
		offset: offset
	});

	$('.board-pannel__filter .dropdown-menu a').on('click', function(){
		var text = $(this).text();
		$(this).parents('.dropdown-menu').prev('a').children('span:first').html(text);
	});

	$('.activity-tabs a').on('click', function(){
		var target = $(this).attr('data-target');
		 $(this).parent('li').addClass('active').siblings('li').removeClass('active');
		$('.pannel.active').fadeOut(200).removeClass('active');
		$(target).fadeIn(200).addClass('active');
		event.preventDefault()
	});

	$('.chart').easyPieChart({
		barColor: 		'#f36f21',
		trackColor: 	'#e6e6e6',
		lineCap:		'square',
		rotate: 		180,
		scaleLength: 	0,
		lineWidth:		10,
		size:			90
	});

	function shotTable(){
		var w = Math.ceil($('.board-pannel__shot').width() / 15) - 1;
		$('.board-pannel__shot').find('td').height(w).slice(1,12).width(w);
	}

	shotTable();

	function btnHeight(){
		$('.board-pannel__filter .btn').removeAttr('style');
		var t = $('[role="toolbar"]').height()
		$('.board-pannel__filter .btn').each(function(){
			if ($(window).width() <= 768) {
				$(this).css({
					'line-height': t - 9  + 'px',
					'height': t
				});
			} else if ($(window).width() <= 460) {
				$(this).css({
					'line-height': t - 10  + 'px',
					'height': t
				});
			} else {
				$(this).removeAttr('style');
			}
		});
	}

	btnHeight();

	function paddingTopAll() {
		var pad = $('.header4').height();
		$('.header4').next().css({'margin-top': pad});
	}

	$('.modal .modal-dialog').css('line-height', $(window).height()+'px');

	$('[data-toggle="tooltip"]').tooltip();

	$('a.video').prettyPhoto({default_width: 740,  default_height: 420,});
	var popup_cfg = {};
	if ($(window).width() < 400)  popup_cfg ={default_width: 250, default_height: 200,allow_resize: false};
	$('a.play').prettyPhoto(popup_cfg);

	function twitterHeight() {
		var lifeFrameHeight = $('.life-frame').outerHeight() - 135;
		$('.board-pannel__twitter iframe').attr('height', lifeFrameHeight);
	}

	twitterHeight();

	function leaderboardTdWidth() {
		var w = $('.board-pannel__item.leader .name').width() + $('.board-pannel__item.leader .avatar').width();
		if($(window).width() >= 768) {
			$('.board-pannel__item.header .avatar').width(w);
		} else {
			$('.board-pannel__item.header .avatar').width(42);
		}
	}

	leaderboardTdWidth();

	$(window).load(function(){
		// resizeText();
		btnHeight();

		// Define the style variables
		var $font = "";
		var $font_weight = 'normal';
		var $border_color = '#666';
		var $border_radius = '0';
		var $text_color = '#fff';
		var $link_color = '#24b4ff';
		var $name_color = '#f36f21';
		var $subtext_color = '#fff'; // Colour of any small text
		var $sublink_color = '#fff'; // Colour of smaller links, eg: @user, date, expand/collapse links
		var $avatar_border = '0';
		var $avatar_border_radius = '0';
		var $icon_color = '#24b4ff'; // Color of the reply/retweet/favourite icons
		var $icon_hover_color = '#24b4ff'; // Hover color the reply/retweet/favourite icons
		var $follow_button_link_color = '#5ea9dd';
		var $footer_background = '#646464';
		var $footer_tweetbox_background = '#646464';
		var $footer_tweetbox_textcolor = '#ffffff';
		var $footer_tweetbox_border ='0';
		var $load_more_background ='#f36f21';
		var $load_more_text_color = '#ffffff';

		// Apply the styles
		$('.board-pannel__twitter iframe').contents().find('head').append('<style> .html, body, h1, h2, h3, blockquote, p, ol, ul, li, img, iframe, button, .tweet-box-button{font-family:'+$font+' !important; font-weight:'+$font_weight+' !important; } .timeline{border-radius: ' + $border_radius + '!important; margin: 0; } .thm-dark .retweet-credit,.h-feed, .stats strong{color:' + $text_color + ' !important; } a:not(.follow-button):not(.tweet-box-button):not(.expand):not(.u-url), .load-more{color:' + $link_color + ' ; } .follow-button{color:' + $follow_button_link_color + ' !important; } .timeline-footer{border-radius:0px 0px ' + $border_radius + ' ' + $border_radius + ' !important; background:' + $footer_background + ' !important; } .tweet-box-button{background-color:' + $footer_tweetbox_background + ' !important; color:' + $footer_tweetbox_textcolor + ' !important; border:' + $footer_tweetbox_border + ' !important; } .timeline .stream, .tweet-actions{background: transparent !important; } .tweet-actions{box-shadow: none !important; } .header .avatar{border-radius: '+ $avatar_border_radius + ' !important; border:' + $avatar_border + ' !important; } .p-name {color:'+$text_color+' !important;} span.p-nickname {color:'+$name_color+' !important; } .tweet.customisable-border{border-bottom: 1px solid ' + $border_color + '; } .u-url, .expand{color:'+$sublink_color+' !important; } .load-more, .no-more-pane {background-color:' + $load_more_background + ' !important; color:' + $load_more_text_color + '!important; } .retweet-credit{color:' + $subtext_color + ' !important; } .customisable-border{border: 0;} .var-chromeless .tweet {padding: 12px 10px 10px 70px;} .var-chromeless button.load-more {border: 0; border-radius: 0; margin: 0; width: 100%; display: block;}</style> ');

		leaderboardTdWidth();
		twitterHeight();
		paddingTopAll();
	});

	$(window).resize(function () {
		$('.has-error [data-toggle="tooltip"]').tooltip('show');
		$('.modal .modal-dialog').css('line-height', $(window).height()+'px');
		$('[data-toggle="tooltip"]').tooltip();
		leaderboardTdWidth();
		twitterHeight();
		paddingTopAll();
		shotTable();
		btnHeight();
		// resizeText();

		if ($('.board-pannel__table').height() < $('.board-pannel__table > .board-pannel__table-wrapper').height()) {
			$('.board-pannel__item.leader').css('padding-right', sw + 1);
		}

		if ($(window).width() >= 991) {
			$('.pannel').fadeIn(200).removeAttr('style');
		}
	});

}(jQuery));